<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $codigo_venta
 * @property int|null $cod_cliente
 * @property int|null $cod_producto
 * @property int|null $cantidad
 * @property float|null $base_imponible_unitaria_venta
 * @property string|null $fecha_de_salida
 * @property string|null $referencia_interna_venta
 * @property string|null $concepto_venta
 * @property int|null $baja
 *
 * @property Clientes $codCliente
 * @property Productos $codProducto
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
             'obligatorios'=>[['referencia_interna_venta','concepto_venta','cantidad'], 'required', 'message'=> 'Campo Obligatorio'],
             [['referencia_interna_venta'], 'match', 'pattern' => '/^[0-9]\w+(\s+\w+)*$/i', 'message'=> 'LA REFERENCIA NO ES VÁLIDA'],
            [['concepto_venta'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i',  'message'=> 'EL CONCEPTO NO ES VÁLIDO'],
             [['base_imponible_unitaria_venta'], 'double'],
            'cantidad' => ['cantidad', 'integer', 'message'=> 'LA CANTIDAD HA DE SER UN NÚMERO ENTERO'],
             'baja' => ['baja', 'default', 'value'=>0],
            [['fecha_de_salida'], 'date', 'format' => 'yyyy-MM-dd'],
            [['cod_cliente', 'cod_producto', 'cantidad'], 'integer'],
           
           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_venta' => 'Codigo Venta',
            'cod_cliente' => 'Cod Cliente',
            'cod_producto' => 'Cod Producto',
            'cantidad' => 'Cantidad',
            'base_imponible_unitaria_venta' => 'Base Imponible Unitaria Venta',
            'fecha_de_salida' => 'Fecha De Salida',
            'referencia_interna_venta' => 'Referencia Interna Venta',
            'concepto_venta' => 'Concepto Venta',
            'baja' => 'Baja',
        ];
    }

    /**
     * Gets query for [[CodCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCliente()
    {
        return $this->hasOne(Clientes::className(), ['codigo_cliente' => 'cod_cliente']);
    }

    /**
     * Gets query for [[CodProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'cod_producto']);
    }
}
