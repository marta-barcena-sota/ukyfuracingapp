<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "enlaces_seguimientos".
 *
 * @property int $codigo_enlace_seguimiento
 * @property int|null $codigo_seguimiento
 * @property string|null $enlace_seguimiento
 * @property int|null $baja
 *
 * @property Seguimientos $codigoSeguimiento
 */
class EnlacesSeguimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enlaces_seguimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['baja', 'default', 'value'=>0],
            [['codigo_seguimiento'], 'integer'],
            [['enlace_seguimiento'], 'url'],
            [['codigo_seguimiento', 'enlace_seguimiento'], 'unique', 'targetAttribute' => ['codigo_seguimiento', 'enlace_seguimiento']],
            [['codigo_seguimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Seguimientos::className(), 'targetAttribute' => ['codigo_seguimiento' => 'codigo_seguimiento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_enlace_seguimiento' => 'Codigo Enlace Seguimiento',
            'codigo_seguimiento' => 'Codigo Seguimiento',
            'enlace_seguimiento' => 'Enlace Seguimiento',
            'baja' => 'Baja',
        ];
    }

    /**
     * Gets query for [[CodigoSeguimiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoSeguimiento()
    {
        return $this->hasOne(Seguimientos::className(), ['codigo_seguimiento' => 'codigo_seguimiento']);
    }
}
