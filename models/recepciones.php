<?php

namespace app\models;

use Yii;
use app\models\Pedidos;

class Recepciones extends Pedidos
{
    

     public function rules()
    {
      
        
        return [
             'obligatorios'=>[['tracking_pedido', 'estado_pedido', 'urgencia_pedido', 'moneda_pago',
                 'referencia_interna', 'forma_de_pago'], 'required', 'message'=> 'Campo Obligatorio'],
             'baja' => ['baja', 'default', 'value'=>0],
             'tipo' => ['tipo_pedido', 'default', 'value'=>'R'],
            'ref_interna' => [['referencia_interna'], 'match', 'pattern' => '/^[0-9]\w+$/i'],
            [['codigo_proveedor'], 'integer'],
            'hay_pago_importacion'=>[['hay_pago_importacion'], 'default', 'value'=>1],
      
            ['pago_importacion', 'double'],
            ['comision_bancaria_pago', 'double'],
            [['fecha_suministro'], 'date', 'format' => 'yyyy-MM-dd'],
            'carpeta'=>['carpeta_pedido', 'match', 'pattern'=>'/^[a-z]\w+$/i'],
            [['tracking_pedido', 'moneda_pago',
                'estado_pedido', 'urgencia_pedido', 'forma_de_pago'], 'string', 'max' => 100],
        ];
    }
}