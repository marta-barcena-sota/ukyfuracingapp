<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emails_intermediarios".
 *
 * @property int $codigo_email
 * @property int|null $codigo_intermediario
 * @property string|null $email_intermediario
 * @property int|null $baja
 *
 * @property Intermediarios $codigoIntermediario
 */
class EmailsIntermediarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emails_intermediarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['baja', 'default', 'value'=>0],
            [['codigo_intermediario'], 'integer'],
            [['email_intermediario'], 'email'],
            [['codigo_intermediario', 'email_intermediario'], 'unique', 'targetAttribute' => ['codigo_intermediario', 'email_intermediario']],
            [['codigo_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => Intermediarios::className(), 'targetAttribute' => ['codigo_intermediario' => 'codigo_intermediario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_email' => 'Codigo Email',
            'codigo_intermediario' => 'Codigo Intermediario',
            'email_intermediario' => 'Email Intermediario',
            'baja' => 'Baja',
        ];
    }

    /**
     * Gets query for [[CodigoIntermediario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIntermediario()
    {
        return $this->hasOne(Intermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }
}
