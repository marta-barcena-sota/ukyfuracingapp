<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cambios".
 *
 * @property int $codigo_cambio
 * @property string|null $motivo_cambio
 * @property int|null $baja
 *
 * @property AlmacenesQueGestionanCambios[] $almacenesQueGestionanCambios
 * @property Almacenes[] $codigoAlmacens
 * @property ProductosQueSufrenCambios[] $productosQueSufrenCambios
 * @property Productos[] $codigoProductos
 */
class Cambios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cambios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'obligatorios'=>[['motivo_cambio'], 'required', 'message'=> 'Campo Obligatorio'],
            'baja' => ['baja', 'default', 'value'=>0],
            [['motivo_cambio'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_cambio' => 'Codigo Cambio',
            'motivo_cambio' => 'Motivo Cambio',
            'baja' => 'Baja',
        ];
    }

    /**
     * Gets query for [[AlmacenesQueGestionanCambios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacenesQueGestionanCambios()
    {
        return $this->hasMany(AlmacenesQueGestionanCambios::className(), ['codigo_cambio' => 'codigo_cambio']);
    }

    /**
     * Gets query for [[CodigoAlmacens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlmacens()
    {
        return $this->hasMany(Almacenes::className(), ['codigo_almacen' => 'codigo_almacen'])->viaTable('almacenes_que_gestionan_cambios', ['codigo_cambio' => 'codigo_cambio']);
    }

    /**
     * Gets query for [[ProductosQueSufrenCambios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosQueSufrenCambios()
    {
        return $this->hasMany(ProductosQueSufrenCambios::className(), ['codigo_cambio' => 'codigo_cambio']);
    }

    /**
     * Gets query for [[CodigoProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProductos()
    {
        return $this->hasMany(Productos::className(), ['codigo_producto' => 'codigo_producto'])->viaTable('productos_que_sufren_cambios', ['codigo_cambio' => 'codigo_cambio']);
    }
}
