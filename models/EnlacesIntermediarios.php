<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "enlaces_intermediarios".
 *
 * @property int $codigo_enlace
 * @property int|null $codigo_intermediario
 * @property string|null $enlace_intermediario
 * @property int|null $baja
 *
 * @property Intermediarios $codigoIntermediario
 */
class EnlacesIntermediarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enlaces_intermediarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['baja', 'default', 'value'=>0],
            [['codigo_intermediario'], 'integer'],
            [['enlace_intermediario'], 'url'],
            [['codigo_intermediario', 'enlace_intermediario'], 'unique', 'targetAttribute' => ['codigo_intermediario', 'enlace_intermediario']],
            [['codigo_intermediario'], 'exist', 'skipOnError' => true, 'targetClass' => Intermediarios::className(), 'targetAttribute' => ['codigo_intermediario' => 'codigo_intermediario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_enlace' => 'Codigo Enlace',
            'codigo_intermediario' => 'Codigo Intermediario',
            'enlace_intermediario' => 'Enlace Intermediario',
            'baja' => 'Baja',
        ];
    }

    /**
     * Gets query for [[CodigoIntermediario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIntermediario()
    {
        return $this->hasOne(Intermediarios::className(), ['codigo_intermediario' => 'codigo_intermediario']);
    }
}
