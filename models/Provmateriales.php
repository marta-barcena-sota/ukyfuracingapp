<?php

namespace app\models;

use Yii;
use app\models\Proveedores;

class Provmateriales extends Proveedores
{
    

     public function rules()
    {
      
        
        return [
            'obligatorios'=>[['nombre_proveedor', 'pais_proveedor'], 'required', 'message'=> 'Campo Obligatorio'],
            'baja' => ['baja', 'default', 'value'=>0],
            'tipo_proveedor'=> ['tipo_proveedor', 'default', 'value' => 'MATERIALES'],
            'cif_proveedor'=> [['cif_proveedor'], 'string', 'max'=>100],
            'eori_proveedor'=> [['eori_proveedor'], 'string', 'max'=>100],
            'nombre_proveedor'=>[['nombre_proveedor'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i', 'message'=> 'EL CAMPO HA DE SER UN NOMBRE VÁLIDO'],
            'empresa_proveedor'=>[['tipo_de_empresa_proveedor'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i'],
            'empresa_proveedor'=>[['sociedad_empresa_proveedor'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i'],
            'pais_proveedor' =>[['pais_proveedor'], 'string', 'max'=>2],
            
        
           
        ];
    }
}