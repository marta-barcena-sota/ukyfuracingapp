<?php

namespace app\models;

use Yii;
use app\models\Proveedores;

class Provtransporteform extends Proveedores
{
    

     public function rules()
    {
      
        
        return [
            'obligatorios'=>[['nombre_proveedor', 'pais_proveedor'], 'required', 'message'=> 'Campo Obligatorio'],
            'baja' => ['baja', 'default', 'value'=>0],
            'tipo_proveedor'=> ['tipo_proveedor', 'default', 'value' => 'TRANSPORTE'],
            'cif_proveedor'=> [['cif_proveedor'], 'match', 'pattern' => '/^([a-z])(\d{8})$/i'],
            'eori_proveedor' => [['eori_proveedor'], 'string', 'max' => 50],
            'nombre_proveedor'=>[['nombre_proveedor'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i', 'message'=> 'EL CAMPO HA DE SER UN NOMBRE VÁLIDO'],
            'empresa_proveedor'=>[['tipo_de_empresa_proveedor'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i'],
            'sociedad_proveedor'=>[['sociedad_empresa_proveedor'], 'match', 'pattern' => '/^[a-z]\w+(\s+\w+)*$/i'],
            'pais_proveedor' =>[['pais_proveedor'], 'match', 'pattern' => '/^[a-z]\w+$/i', 'message'=> 'EL PAÍS HA DE SER INTRODUCIDO CON EL SIGUIENTE FORMATO: XX'],
            
        
           
        ];
    }
}