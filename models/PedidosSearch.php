<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pedidos;

/**
 * PedidosSearch represents the model behind the search form of `app\models\Pedidos`.
 */
class PedidosSearch extends Pedidos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'obligatorios'=>[['tracking_pedido', 'referencia_interna', 'tipo_pedido', 'estado_pedido', 'urgencia_pedido', 
                'forma_de_pago'], 'required', 'message'=> 'Campo Obligatorio'],
            'baja' => ['baja', 'default', 'value'=>0],
            'carpeta_pedido' => ['carpeta_pedido', 'default', 'value'=>0],
            'ref_interna' => [['referencia_interna'], 'match', 'pattern' => '/^[0-9]\w+$/i'],
            'hay_pago_importacion'=>[['hay_pago_importacion'], 'integer'],
            'ajenas'=>[['codigo_proveedor', 'codigo_cliente'], 'integer'],
            'pagos'=>[['pago_importacion', 'comision_bancaria_pago'], 'double'],
            [['fecha_encargo'], 'date', 'format' => 'yyyy-MM-dd'],
            [['fecha_suministro'], 'date', 'format' => 'yyyy-MM-dd'],
            [['tracking_pedido',  'tipo_pedido', 'estado_pedido', 'urgencia_pedido', 'moneda_pago', 'forma_de_pago'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pedidos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_pedido' => $this->codigo_pedido,
            'codigo_proveedor' => $this->codigo_proveedor,
            'codigo_cliente' => $this->codigo_cliente,
            'hay_pago_importacion' => $this->hay_pago_importacion,
            'pago_importacion' => $this->pago_importacion,
            'comision_bancaria_pago' => $this->comision_bancaria_pago,
            'fecha_encargo' => $this->fecha_encargo,
            'fecha_suministro' => $this->fecha_suministro,
            'baja' => $this->baja,
        ]);

        $query->andFilterWhere(['like', 'tracking_pedido', $this->tracking_pedido])
            ->andFilterWhere(['like', 'carpeta_pedido', $this->carpeta_pedido])
            ->andFilterWhere(['like', 'tipo_pedido', $this->tipo_pedido])
            ->andFilterWhere(['like', 'estado_pedido', $this->estado_pedido])
            ->andFilterWhere(['like', 'urgencia_pedido', $this->urgencia_pedido])
            ->andFilterWhere(['like', 'moneda_pago', $this->moneda_pago])
            ->andFilterWhere(['like', 'forma_de_pago', $this->forma_de_pago])
            ->andFilterWhere(['like', 'referencia_interna', $this->referencia_interna]);

        return $dataProvider;
    }
}
