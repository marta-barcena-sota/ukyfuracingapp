<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documentaciones".
 *
 * @property int $codigo_documentacion
 * @property int|null $codigo_pedido
 * @property int|null $codigo_producto
 * @property string|null $documento_de_transporte
 * @property string|null $justificante_de_pago
 * @property int|null $numero_de_factura
 * @property string|null $fecha_factura
 * @property string|null $archivo_factura
 * @property string|null $albaran
 * @property string|null $pedido
 * @property string|null $documento_importacion
 * @property int|null $baja
 *
 * @property Pedidos $codigoPedido
 * @property Productos $codigoProducto
 */
class Documentaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documentaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_pedido', 'codigo_producto', 'numero_de_factura'], 'integer'],
              'baja' => ['baja', 'default', 'value'=>0],
            [['fecha_factura'], 'date', 'format' => 'yyyy-MM-dd'],
            [['documento_de_transporte', 'justificante_de_pago', 'archivo_factura', 'albaran', 'pedido', 'documento_importacion'], 'string', 'max' => 100],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_documentacion' => 'Codigo Documentacion',
            'codigo_pedido' => 'Codigo Pedido',
            'codigo_producto' => 'Codigo Producto',
            'documento_de_transporte' => 'Documento De Transporte',
            'justificante_de_pago' => 'Justificante De Pago',
            'numero_de_factura' => 'Numero De Factura',
            'fecha_factura' => 'Fecha Factura',
            'archivo_factura' => 'Archivo Factura',
            'albaran' => 'Albaran',
            'pedido' => 'Pedido',
            'documento_importacion' => 'Documento Importacion',
            'baja' => 'Baja',
        ];
    }

    /**
     * Gets query for [[CodigoPedido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPedido()
    {
        return $this->hasOne(Pedidos::className(), ['codigo_pedido' => 'codigo_pedido']);
    }

    /**
     * Gets query for [[CodigoProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoProducto()
    {
        return $this->hasOne(Productos::className(), ['codigo_producto' => 'codigo_producto']);
    }
}
