<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tlfs_empleados".
 *
 * @property int $codigo_tlf
 * @property int|null $codigo_empleado
 * @property string|null $tlf_empleado
 * @property int|null $baja
 *
 * @property Empleados $codigoEmpleado
 */
class TlfsEmpleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tlfs_empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'baja' => ['baja', 'default', 'value'=>0],
            [['codigo_empleado', 'baja'], 'integer'],
            [['tlf_empleado'], 'string', 'max' => 100],
            [['codigo_empleado', 'tlf_empleado'], 'unique', 'targetAttribute' => ['codigo_empleado', 'tlf_empleado']],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_tlf' => 'Codigo Tlf',
            'codigo_empleado' => 'Codigo Empleado',
            'tlf_empleado' => 'Tlf Empleado',
            'baja' => 'Baja',
        ];
    }

    /**
     * Gets query for [[CodigoEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['codigo_empleado' => 'codigo_empleado']);
    }
}
