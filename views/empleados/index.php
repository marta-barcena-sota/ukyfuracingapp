<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmpleadosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-index">
    
       <br><!-- comment -->
        <br><!-- comment -->
          <h1><?= Html::encode($this->title) ?></h1>
  
                  <br><!-- comment -->
         
             <?= Html::a('NUEVO EMPLEADO', ['empleados/create'], ['class' => 'btn btn-marta']) ?>

         <br><!-- comment -->
               <br><!-- comment -->
    
    
  
</div>

        
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_empleado',
            'nombre_empleado',
            'primer_apellido_empleado',
            'segundo_apellido_empleado',
//            'puesto_empleado',
            //'baja',

            ['class' => 'yii\grid\ActionColumn'],
        ],
//        'options' => [ 'style' => 'background-color: whitesmoke; color:black;' ]
    ]); ?>
 
