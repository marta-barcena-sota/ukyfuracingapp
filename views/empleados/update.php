<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Empleados */

$this->title = 'Update Empleados: ' . $model->codigo_empleado;
$this->params['breadcrumbs'][] = ['label' => 'EMPLEADOS', 'url' => ['empleados/empleadosenalta']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
