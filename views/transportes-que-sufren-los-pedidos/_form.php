<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Transportes;
use app\models\Pedidos;

/* @var $this yii\web\View */
/* @var $model app\models\TransportesQueSufrenLosPedidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transportes-que-sufren-los-pedidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $pedidos= Pedidos::find()->where('baja=0')->all();
$datapedidos=ArrayHelper::map($pedidos, 'codigo_pedido', 'referencia_interna');
echo $form->field($model, 'codigo_pedido', [
    "template" => "<label> PEDIDO </label>\n{input}\n{hint}\n{error}"
])->dropDownList($datapedidos, ['prompt'=> 'ELIGE UN PEDIDO']) ?>

    <?php $transportes= Transportes::find()->where('baja=0')->all();
$datatransportes=ArrayHelper::map($transportes, 'codigo_transporte', 'descripcion_transporte');
echo $form->field($model, 'codigo_transporte', [
    "template" => "<label> TRANSPORTE </label>\n{input}\n{hint}\n{error}"
])->dropDownList($datatransportes, ['prompt'=> 'ELIGE UN TRANSPORTE']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
