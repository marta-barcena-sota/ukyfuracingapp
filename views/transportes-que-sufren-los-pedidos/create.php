<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TransportesQueSufrenLosPedidos */

$this->title = 'ALTA DE TRANSPORTE QUE SUFRE UN PEDIDO';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = ['label' => 'PEDIDOS', 'url' => ['pedidos/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transportes-que-sufren-los-pedidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
