<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Empleados;
use app\models\Pedidos;
use app\models\Almacenes;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueGestionanPedidosEnAlmacenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleados-que-gestionan-pedidos-en-almacenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $empleados=Empleados::find()-> where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado', [
    "template" => "<label> Empleado </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

   <?php $pedidos= Pedidos::find()->where('baja=0')->all();
$datapedidos=ArrayHelper::map($pedidos, 'codigo_pedido', 'concepto_pedido');
echo $form->field($model, 'codigo_pedido')->dropDownList($datapedidos, ['prompt'=> 'ELIGE UN PEDIDO'])?>

   <?php $almacenes= Almacenes::find()->where('baja=0')->all();
$dataalmacenes=ArrayHelper::map($almacenes, 'codigo_almacen', 'denominacion_almacen');
echo $form->field($model, 'codigo_almacen')->dropDownList($dataalmacenes, ['prompt'=> 'ELIGE UN ALMACÉN'])?>

    <?= $form->field($model, 'inventario', ['template' => '{label}
*{input}'])->dropDownList(
            [0=>'NO',
                1=> 'SÍ',
            ]
            , ['prompt'=> '¿ESTÁ AÑADIDO AL INVENTARIO?']) ?>

  <?= $form->field($model, 'contabilidad', ['template' => '{label}
*{input}'])->dropDownList(
            [0=>'NO',
                1=> 'SÍ',
            ]
            , ['prompt'=> '¿ESTÁ AÑADIDO A LA CONTABILIDAD?']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
