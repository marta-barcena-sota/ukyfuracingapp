<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueGestionanPedidosEnAlmacenes */

$this->title = 'Create Empleados Que Gestionan Pedidos En Almacenes';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'ALMACENES', 'url' => ['almacenes/almaceneslogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="empleados-que-gestionan-pedidos-en-almacenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
