<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;
use app\models\Clientes;

use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedidos-form">

    <?php $form = ActiveForm::begin(); ?>
    
     <?= $form->field($model, 'tipo_pedido', ['template' => '{label}
*{input}'])->dropDownList(
            ['R'=>'RECEPCIÓN',
                'E'=> 'ENVÍO',
            ]
            , ['prompt'=> 'ELIGE UN TIPO DE PEDIDO']) ?>
    
        <?= $form->field($model, 'tracking_pedido', ['template' => '{label}
<span title="Único para cada pedido" class="glyphicon glyphicon-info-sign"></span> *{input}'])->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'referencia_interna', ['template' => '{label}
<span title="Referencia impuesta. Cumple el siguiente formato:
AÑO(NºORDEN) 
Ejemplo: 202102" class="glyphicon glyphicon-info-sign"></span> * {input}'])->textInput(['maxlength' => true]) ?>
    
        <?php $clientes=Clientes::find()->where('baja=0')->all();
$dataclientes=ArrayHelper::map($clientes, 'codigo_cliente', 'nombre_cliente');
    echo  $form->field($model, 'codigo_cliente', [
    "template" => "<label> Cliente </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataclientes, ['prompt'=> 'ELIGE UN CLIENTE']) ?>
      
    <?php $proveedores=Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
    echo  $form->field($model, 'codigo_proveedor', [
    "template" => "<label> Proveedor </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR']) ?>
    

    <?= $form->field($model, 'estado_pedido', ['template' => '{label}
<span title="Uno de los estados predeterminados.

- Previo a proceso: Cuando es algo que se quiere comprar pero no está seleccionado. Pendiente de
recibir presupuesto por ejemplo, tamién entra aquí.

- Nueva orden: Cuando Mario o Pelayo generan una nueva orden. Debe ir acompañada de un documento de pedido
(presupuesto, factura, proforma, pedido, etc.)

- Aceptada: Cuando Alberto ve la orden y la acepta.

-En proceso: Cuando el periodo está en manos del proveedor o Alberto genera una nueva orden.

- Pendiente de pago.

- Enviado.

- Recibido.

- Pendiente de factura.

- Completado: Cuando, una vez recibido; se comprueba que ha venido todo,
está todo bien, el albarán se corresponde con la factura y se añade al inventario.
A su vez, todos los campos están completados." class="glyphicon glyphicon-info-sign"></span> *{input}'])->dropDownList(
            ['PREVIO A PROCESO'=>'PREVIO A PROCESO',
                'NUEVA ORDEN'=> 'NUEVA ORDEN',
                'ACEPTADA'=>'ACEPTADA',
                'EN PROCESO'=>'EN PROCESO',
                ' PENDIENTE DE PAGO'=> 'PENDIENTE DE PAGO',
                'ENVIADO'=> 'ENVIADO',
                'RECIBIDO'=>'RECIBIDO',
                'PDTE FACTURA'=> 'PDTE FACTURA',
                'COMPLETADO'=> 'COMPLETADO']
            , ['prompt'=> 'ELIGE UN ESTADO']) ?>

    <?= $form->field($model, 'urgencia_pedido', ['template' => '{label}
*{input}'])->dropDownList(
            ['PRIORIDAD ALTA'=>'PRIORIDAD ALTA',
                'PRIORIDAD MEDIA'=> 'PRIORIDAD MEDIA',
                'PRIORIDAD BAJA'=>'PRIORIDAD BAJA',
            ]
            , ['prompt'=> 'ELIGE UNA URGENCIA']) ?>
    

    <?= $form->field($model, 'forma_de_pago', ['template' => '{label}
*{input}'])->dropDownList(
            ['TRANSFERENCIA'=>'TRANSFERENCIA',
                'DOMICILIADO'=> 'DOMICILIADO',
                'PAYPAL'=>'PAYPAL',
                'TARJETA ****9290'=> 'TARJETA ****9290',
                'TARJETA ****4504'=>'TARJETA ****4504',
                'EFECTIVO'=> 'EFECTIVO'
            ]
            , ['prompt'=> 'ELIGE UNA FORMA DE PAGO']) ?>
    
     <?= $form->field($model, 'moneda_pago')->textInput(['maxlength' => true]) ?>

    
     
 
      <?= $form->field($model, 'pago_importacion', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>
    
      <?= $form->field($model, 'comision_bancaria_pago', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>
    
       <?= $form->field($model, 'fecha_encargo')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>
    
       <?= $form->field($model, 'fecha_suministro')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
