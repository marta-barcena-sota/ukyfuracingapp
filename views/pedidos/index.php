<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PedidosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pedidos';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedidos-index">
    <br><!-- comment -->
    <br><!-- comment -->
    <h1><?= Html::encode($this->title) ?></h1>
   <br><!-- comment -->
    <br><!-- comment -->
    <p>
        <?= Html::a('NUEVO PEDIDO', ['create'], ['class' => 'btn btn-marta']) ?>
         <?= Html::a('AÑADIR TRANSPORTE A PEDIDO', ['transportes-que-sufren-los-pedidos/create'], ['class' => 'btn btn-marta']) ?>
         <?= Html::a('AÑADIR SEGUIMIENTO A PEDIDO', ['seguimientos/createpedido'], ['class' => 'btn btn-marta']) ?>
        <?= Html::a('AÑADIR PRODUCTO A PEDIDO', ['productos-que-componen-pedidos/create'], ['class' => 'btn btn-marta']) ?>
      <?= Html::a('AÑADIR COMENTARIO A PEDIDO', ['comentarios/createpedidos'], ['class' => 'btn btn-marta']) ?>
    </p>
   <br><!-- comment -->
    <br><!-- comment -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_pedido',
//            'codigo_proveedor',
//            'codigo_cliente',
            'tracking_pedido',
//            'carpeta_pedido',
            //'tipo_pedido',
            //'estado_pedido',
            //'urgencia_pedido',
            //'moneda_pago',
            //'forma_de_pago',
            //'hay_pago_importacion',
            //'pago_importacion',
            //'comision_bancaria_pago',
            //'fecha_encargo',
            //'fecha_suministro',
            //'baja',
            'referencia_interna',

               ['class'=>'yii\grid\ActionColumn']
        ],
    ]); ?>


</div>
