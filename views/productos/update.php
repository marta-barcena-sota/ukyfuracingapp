<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Productos */

$this->title = 'Update Productos: ' . $model->referencia_interna_producto;
$this->params['breadcrumbs'][] = ['label' => 'LOGISTICA', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'INVENTARIO', 'url' => ['productos/inventariologistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
