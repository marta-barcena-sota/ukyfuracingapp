<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Productos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>
    
      <?= $form->field($model, 'referencia_interna_producto', ['template' => '{label}
<span title="Referencia impuesta. Cumple el siguiente formato:
AÑO(NºORDEN) 
Ejemplo: 202102" class="glyphicon glyphicon-info-sign"></span> *{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'referencia_articulo_producto', ['template' => '{label}
<span title="Referencia original del artículo." class="glyphicon glyphicon-info-sign"></span> *{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cantidad_en_stock', ['template' => '{label}
*{input}'])->textInput() ?>

    <?= $form->field($model, 'forma_de_almacenamiento', ['template' => '{label}
<span title="Cómo está almacenado el producto.
Ejemplo: Cajas de 12 unidades." class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'concepto_producto', ['template' => '{label}
*{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primera_categoria_producto', ['template' => '{label}
*{input}'])->dropdownList([
        'EXISTENCIAS'=>'EXISTENCIAS', 
        'INMOVILIZADO MATERIAL'=>'INMOVILIZADO MATERIAL',
    ],
    ['prompt'=>'Seleccione una categoría']
) ?>

    <?= $form->field($model, 'segunda_categoria_producto', ['template' => '{label}
*{input}'])->dropdownList([
        'MATERIAS PRIMAS'=>'MATERIAS PRIMAS', 
        'PRODUCTOS SEMIELABORADOS'=>'PRODUCTOS SEMIELABORADOS',
        'PRODUCTOS ACABADOS'=>'PRODUCTOS ACABADOS',
        'EXISTENCIAS COMERCIALES'=>'EXISTENCIAS COMERCIALES',
        'OTROS APROVISIONAMIENTOS'=>'OTROS APROVISIONAMIENTOS',
        'SUBPRODUCTOS'=>'SUBPRODUCTOS',
        'MOBILIARIO'=> 'MOBILIARIO',
        'MAQUINARIA'=> 'MAQUINARIA',
        'HERRAMIENTAS'=> 'HERRAMIENTAS',
        'EQUIPOS INFORMÁTICOS'=> 'EQUIPOS INFORMÁTICOS',
        'OTROS'=>'OTROS',
        
    ],
    ['prompt'=>'Seleccione una categoría']
) ?>

    <?= $form->field($model, 'tercera_categoria_producto')->textInput(['maxlength' => true]) ?>

  


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
