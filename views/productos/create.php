<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Productos */

$this->title = 'ALTA DE PRODUCTO EN INVENTARIO';
$this->params['breadcrumbs'][] = ['label' => 'LOGISTICA', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'INVENTARIO', 'url' => ['productos/inventariologistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
