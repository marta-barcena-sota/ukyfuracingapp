<?php

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */

use yii\helpers\Html;

$this->title = 'INVENTARIO: PAPELERA';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'INVENTARIO', 'url' => ['productos/inventariologistica']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
        <br>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/inventario.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>INVENTARIO : PAPELERA</h1>
                    
                </div>
                
            </div>
        </div>
        <br>
        <br>
         
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_producto',
         'referencia_articulo_producto',
         'referencia_interna_producto',
         'concepto_producto',
//         'primera_categoria_producto',
//         'segunda_categoria_producto',
//         'tercera_categoria_producto',
//         'cantidad_en_stock',
//         'forma_de_almacenamiento',
                                                 ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {alta}, {delete}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'alta' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-ok"></span>',$url, [
                            'title' => Yii::t('app', 'lead-alta'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            },
                    'delete' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url, [
                            'title' => Yii::t('app', 'lead-delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_producto'];

        	return $url;

    	}
        
         if ($action === 'alta') {

        	$url = './alta?id='.$model['codigo_producto'];

        	return $url;

    	}
        if ($action === 'delete') {

        	$url = './delete?id='.$model['codigo_producto'];

        	return $url;

    	}


	}]
         ],
      
        ]);?>
