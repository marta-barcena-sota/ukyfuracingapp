<?php

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */

use yii\helpers\Html;

$this->title = 'INVENTARIO';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
        <br>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/inventario.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>INVENTARIO</h1>
                    
                </div>
                
            </div>
        </div>
        <br>
        <br>
         <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('Registrar un nuevo artículo', ['create'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['productos/inventariologisticabajas'], ['class' => 'btn btn-marta']) ?>
                 
    </p>    
            </div>
        </div>
        <br>
        <br>
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_producto',
         'referencia_articulo_producto',
         'referencia_interna_producto',
         'concepto_producto',
//         'primera_categoria_producto',
//         'segunda_categoria_producto',
//         'tercera_categoria_producto',
//         'cantidad_en_stock',
//         'forma_de_almacenamiento',
                         ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_producto'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_producto'];

        	return $url;

    	}

	}]
         ],
      
        ]);?>
