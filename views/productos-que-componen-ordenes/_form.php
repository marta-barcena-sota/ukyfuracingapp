<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Productos;
use app\models\OrdenesDeFabricacion;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-que-componen-ordenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $productos= Productos::find()->where('baja=0')->all();
$dataproductos=ArrayHelper::map($productos, 'codigo_producto', 'concepto_producto');
echo $form->field($model, 'codigo_producto')->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PRODUCTO'])?>

   <?php $ordenes= OrdenesDeFabricacion::find()->where('baja=0')->all();
$dataordenes=ArrayHelper::map($ordenes, 'codigo_orden_de_fabricacion', 'nombre_orden_de_fabricacion');
echo $form->field($model, 'codigo_orden')->dropDownList($dataordenes, ['prompt'=> 'ELIGE UNA ORDEN DE FABRICACIÓN'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
