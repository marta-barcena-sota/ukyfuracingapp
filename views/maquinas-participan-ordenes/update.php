<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MaquinasParticipanOrdenes */

$this->title = 'Update Maquinas Participan Ordenes: ' . $model->codigo_operacion;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'ÓRDENES DE FABRICACIÓN', 'url' => ['ordenes-de-fabricacion/ordenesproduccion']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinas-participan-ordenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
