<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Maquinas;
use app\models\OrdenesDeFabricacion;

/* @var $this yii\web\View */
/* @var $model app\models\MaquinasParticipanOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maquinas-participan-ordenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $maquinas= Maquinas::find()->where('baja=0')->all();
$datamaquinas=ArrayHelper::map($maquinas, 'codigo_maquina', 'nombre_maquina');
echo $form->field($model, 'codigo_maquina')->dropDownList($datamaquinas, ['prompt'=> 'ELIGE UNA MÁQUINA'])?>

     <?php $ordenes= OrdenesDeFabricacion::find()->where('baja=0')->all();
$dataordenes=ArrayHelper::map($ordenes, 'codigo_orden_de_fabricacion', 'nombre_orden_de_fabricacion');
echo $form->field($model, 'codigo_orden')->dropDownList($dataordenes, ['prompt'=> 'ELIGE UNA ORDEN DE FABRICACIÓN'])?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
