<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;
use app\models\Materiales;
use app\models\OrdenesDeFabricacion;
/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresMaterialesOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-materiales-ordenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])?>

     <?php $materiales= Materiales::find()->where('baja=0')->all();
$datamateriales=ArrayHelper::map($materiales, 'codigo_material', 'familia_material');
echo $form->field($model, 'codigo_material')->dropDownList($datamateriales, ['prompt'=> 'ELIGE UN MATERIAL'])?>

    <?php $ordenes= OrdenesDeFabricacion::find()->where('baja=0')->all();
$dataordenes=ArrayHelper::map($ordenes, 'codigo_orden_de_fabricacion', 'nombre_orden_de_fabricacion');
echo $form->field($model, 'codigo_orden')->dropDownList($dataordenes, ['prompt'=> 'ELIGE UNA ORDEN DE FABRICACIÓN'])?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'medida')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
