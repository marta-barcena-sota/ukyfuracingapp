<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proyectos;
use app\models\Intermediarios;
/* @var $this yii\web\View */
/* @var $model app\models\IntermediariosQueParticipanEnProyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intermediarios-que-participan-en-proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php $intermediarios= Intermediarios::find()->where('baja=0')->all();
$dataintermediarios=ArrayHelper::map($intermediarios, 'codigo_intermediario', 'nombre_intermediario');
echo $form->field($model, 'codigo_intermediario')->dropDownList($dataintermediarios, ['prompt'=> 'ELIGE UN INTERMEDIARIO'])?>
     <?php $proyectos= Proyectos::find()->where('baja=0')->all();
$dataproyectos=ArrayHelper::map($proyectos, 'codigo_proyecto', 'nombre_proyecto');
echo $form->field($model, 'codigo_proyecto')->dropDownList($dataproyectos, ['prompt'=> 'ELIGE UN PROYECTO'])?>

  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
