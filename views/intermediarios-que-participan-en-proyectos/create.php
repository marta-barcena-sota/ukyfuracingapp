<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IntermediariosQueParticipanEnProyectos */

$this->title = 'Create Intermediarios Que Participan En Proyectos';
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'PROYECTOS', 'url' => ['proyectos/proyectosproduccion']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intermediarios-que-participan-en-proyectos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
