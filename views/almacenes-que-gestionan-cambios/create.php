<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGestionanCambios */

$this->title = 'Create Almacenes Que Gestionan Cambios';
$this->params['breadcrumbs'][] = ['label' => 'Almacenes Que Gestionan Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="almacenes-que-gestionan-cambios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
