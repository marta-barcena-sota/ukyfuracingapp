<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Almacenes;
use yii\helpers\ArrayHelper;
use app\models\Cambios;

/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGestionanCambios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="almacenes-que-gestionan-cambios-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php $almacenes= Almacenes::find()->where('baja=0')->all();
$dataalmacenes=ArrayHelper::map($almacenes, 'codigo_almacen', 'denominacion_almacen');
echo $form->field($model, 'codigo_almacen')->dropDownList($dataalmacenes, ['prompt'=> 'ELIGE UN ALMACÉN'])?>

     <?php $cambios= Cambios::find()->where('baja=0')->all();
$datacambios=ArrayHelper::map($cambios, 'codigo_cambio', 'motivo_cambio');
echo $form->field($model, 'codigo_cambio')->dropDownList($datacambios, ['prompt'=> 'ELIGE UN CAMBIO'])?>
    <?= $form->field($model, 'origen', ['template' => '{label}
*{input}'])->dropDownList(
            [0=>'NO',
               1=> 'SÍ',
            ]
            , ['prompt'=> '¿ES EL ORIGEN?']) ?>

   <?= $form->field($model, 'destino', ['template' => '{label}
*{input}'])->dropDownList(
            [0=>'NO',
               1=> 'SÍ',
            ]
            , ['prompt'=> '¿ES EL DESTINO?']) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
