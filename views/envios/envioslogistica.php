<?php

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */


use yii\helpers\Html;

$this->title = 'ENVÍOS';
$this->params['breadcrumbs'][] = ['label' => 'LOGÍSTICA', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="body-content">
        <br><!-- comment -->
        <br><!-- comment -->
            
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/bdgral.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>BD GRAL: ENVÍOS</h1>
                    
                </div>
                
            </div>
        </div>
        
            <br><!-- comment -->
        <br><!-- comment -->
            
        
        <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('NUEVO ENVÍO', ['envios/createenvios'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['envios/envioslogisticapapelera'], ['class' => 'btn btn-marta']) ?>
                  
    </p>    
            </div>
        </div>
        
            <br><!-- comment -->
        <br><!-- comment -->
            
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_pedido',
         'tracking_pedido',
         'tipo_pedido',
         'estado_pedido',
         'urgencia_pedido',
         'forma_de_pago',
     ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                   'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            } ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_pedido'];

        	return $url;

    	}
                if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_pedido'];

        	return $url;

    	}


	}]
         
        ]]);?>

