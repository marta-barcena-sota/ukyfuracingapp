<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */

$this->title = $model->referencia_interna;
$this->params['breadcrumbs'][] = ['label' => 'LOGÍSTICA', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = ['label' => 'ENVÍOS', 'url' => ['envios/envioslogistica']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedidos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_pedido], ['class' => 'btn btn-marta']) ?>
<!--        Html::a('Delete', ['delete', 'id' => $model->codigo_pedido], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_pedido',
            [
                'attribute' => 'codigo_cliente',
                'label' => 'CLIENTE',
                'value' => $model->codigoCliente->nombre_cliente,
            ],
//            'codigo_proveedor',
//            'codigo_cliente',
            'tracking_pedido',
            'carpeta_pedido',
            'tipo_pedido',
            'estado_pedido',
            'urgencia_pedido',
            'moneda_pago',
            'forma_de_pago',
//            'hay_pago_importacion',
            'pago_importacion',
            'comision_bancaria_pago',
//            'fecha_encargo',
            'fecha_suministro',
//            'baja',
            'referencia_interna',
        ],
    ]) ?>

</div>