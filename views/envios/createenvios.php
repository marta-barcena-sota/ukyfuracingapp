<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recepciones */

$this->title = 'ALTA ENVÍO';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = ['label' => 'ENVÍOS', 'url' => ['envios/envioslogistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formenvios', [
        'model' => $model,
    ]) ?>

</div>
