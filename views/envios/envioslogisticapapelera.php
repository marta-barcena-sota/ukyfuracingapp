<?php

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */


use yii\helpers\Html;

$this->title = 'ENVÍOS: PAPELERA';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = ['label' => 'ENVÍOS', 'url' => ['envios/envioslogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
            <br><!-- comment -->
        <br><!-- comment -->
            
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/bdgral.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>ENVÍOS: PAPELERA</h1>
                    
                </div>
                
            </div>
        </div>
            <br><!-- comment -->
        <br><!-- comment -->
            <br><!-- comment -->
        <br><!-- comment -->
                <br><!-- comment -->
        <br><!-- comment -->
            
            
       
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_pedido',
         'tracking_pedido',
         'tipo_pedido',
         'estado_pedido',
         'urgencia_pedido',
         'forma_de_pago',
     ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {alta}, {delete}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                   'alta' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-ok"></span>',$url, [
                            'title' => Yii::t('app', 'lead-alta'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            },
                    'delete' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url, [
                            'title' => Yii::t('app', 'lead-delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method'  => 'post']);
            } ],
                            
                    
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_pedido'];

        	return $url;

    	}
                if ($action === 'alta') {

        	$url = './alta?id='.$model['codigo_pedido'];

        	return $url;

    	}
        if ($action === 'delete') {

        	$url = './delete?id='.$model['codigo_pedido'];

        	return $url;

    	}


	}]
         
        ]]);?>