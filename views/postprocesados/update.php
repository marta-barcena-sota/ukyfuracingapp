<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Postprocesados */

$this->title = 'Update Postprocesados: ' . $model->codigo_postprocesado;
$this->params['breadcrumbs'][] = ['label' => 'Postprocesados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_postprocesado, 'url' => ['view', 'id' => $model->codigo_postprocesado]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="postprocesados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
