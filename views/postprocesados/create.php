<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Postprocesados */

$this->title = 'Create Postprocesados';
$this->params['breadcrumbs'][] = ['label' => 'Postprocesados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="postprocesados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
