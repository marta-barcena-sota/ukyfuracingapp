<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Postprocesados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postprocesados-form">

    <?php $form = ActiveForm::begin(); ?>

     <?= $form->field($model, 'fecha_inicio_postprocesado')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?> 

     <?= $form->field($model, 'fecha_fin_postprocesado')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?> 

    <?= $form->field($model, 'descripcion_postprocesado')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
