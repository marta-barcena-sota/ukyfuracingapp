<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedores */

$this->title = $model->nombre_proveedor;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'PROVEEDORES DE POSTPROCESADOS', 'url' => ['proveedores/provpostprocesadosproduccion']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="proveedores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_proveedor], ['class' => 'btn btn-marta']) ?>
<!--         Html::a('Delete', ['delete', 'id' => $model->codigo_proveedor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_proveedor',
            'cif_proveedor',
            'eori_proveedor',
            'nombre_proveedor',
            'tipo_de_empresa_proveedor',
            'sociedad_empresa_proveedor',
            'pais_proveedor',
            'tipo_proveedor',
            'baja',
        ],
    ]) ?>

</div>
