<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Empleados;
use app\models\Maquinas;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Mantenimientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mantenimientos-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php $empleados=Empleados::find()-> where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado', [
    "template" => "<label> Empleado </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

   <?php $maquinas= Maquinas::find()->where('baja=0')->all();
$datamaquinas=ArrayHelper::map($maquinas, 'codigo_maquina', 'nombre_maquina');
echo $form->field($model, 'codigo_maquina')->dropDownList($datamaquinas, ['prompt'=> 'ELIGE UNA MÁQUINA'])?>

    <?= $form->field($model, 'version_firmware')->textInput(['maxlength' => true]) ?>

       <?= $form->field($model, 'fecha_de_realizacion_mantenimiento')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
