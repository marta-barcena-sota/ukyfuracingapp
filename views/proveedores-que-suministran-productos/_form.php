<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;
use app\models\Productos;
/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranProductos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-suministran-productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])?>

   <?php $productos= Productos::find()->where('baja=0')->all();
$dataproductos=ArrayHelper::map($proveedores, 'codigo_producto', 'concepto_producto');
echo $form->field($model, 'codigo_producto')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PRODUCTO'])?>

    <?= $form->field($model, 'base_imponible_unitaria')->textInput() ?>

    <?= $form->field($model, 'iva_impuesto')->textInput() ?>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
