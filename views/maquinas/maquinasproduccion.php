<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = 'MAQUINAS';
$this->params['breadcrumbs'][] = ['label' => 'PRODUCCIÓN', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="body-content">
    <br><!-- comment -->
    <br><!-- comment -->
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/maquinas.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>MAQUINAS</h1>
                    
                </div>
                
            </div>
        </div>
       <br><!-- comment -->
    <br><!-- comment -->
           <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('Registrar una nueva máquina', ['maquinas/create'], ['class' => 'btn btn-marta']) ?>
          
                      <?= Html::a('Papelera', ['maquinas/maquinasenbaja'], ['class' => 'btn btn-marta']) ?>
    </p>    
            </div>
        </div>
    <br><!-- comment -->
    <br><!-- comment -->
    </div>

<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_maquina',
                'nombre_maquina',
       ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_maquina'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_maquina'];

        	return $url;

    	}

	}]
         ],
        ]);?>
