<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsClientes */

$this->title = 'Update Emails Clientes: ' . $model->codigo_email;
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = ['label' => 'EMAILS CLIENTES', 'url' => ['emails-clientes/emailsclientes']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emails-clientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
