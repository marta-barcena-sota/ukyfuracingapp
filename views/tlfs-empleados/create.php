<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsEmpleados */

$this->title = 'ALTA TLF EMPLEADO';
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = ['label' => 'TLFS EMPLEADOS', 'url' => ['tlfs-empleados/empleadostlfs']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tlfs-empleados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
