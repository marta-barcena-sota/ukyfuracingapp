<?php

/* @var $this yii\web\View */
/* @var $model app\models\Ventas */
use yii\helpers\Html;

$this->title = 'TLFS EMPLEADOS';
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
        <br>
        <br>
        <div class="row">
        
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>TLFS EMPLEADOS</h1>
                    
                </div>
                
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('Registrar teléfono', ['create'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['tlfs-empleados/empleadostlfsbajas'], ['class' => 'btn btn-marta']) ?>
    </p>    
            </div>
        </div>
        <br>
        <br>
    </div>
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
         'tlf_empleado',
        
                             ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_tlf'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_tlf'];

        	return $url;

    	}

	}]
         ],
      
        ]);?>
