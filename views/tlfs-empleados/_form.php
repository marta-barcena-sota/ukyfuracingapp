<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Empleados;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsEmpleados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tlfs-empleados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $empleados= Empleados::find()->where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado')->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

    <?= $form->field($model, 'tlf_empleado')->textInput(['maxlength' => true]) ?>

 

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
