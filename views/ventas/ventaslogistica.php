<?php

/* @var $this yii\web\View */
/* @var $model app\models\Ventas */
use yii\helpers\Html;

$this->title = 'VENTAS';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
        <br>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/ventas.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>VENTAS</h1>
                    
                </div>
                
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('Registrar nueva venta', ['create'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['ventas/ventaslogisticabajas'], ['class' => 'btn btn-marta']) ?>
    </p>    
            </div>
        </div>
        <br>
        <br>
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
         'referencia_interna_venta',
         'concepto_venta',
                             ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_venta'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_venta'];

        	return $url;

    	}

	}]
         ],
      
        ]);?>
