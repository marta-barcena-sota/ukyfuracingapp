<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ventas */

$this->title = 'ALTA DE VENTA';
$this->params['breadcrumbs'][] = ['label' => 'LOGÍSTICA', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'VENTAS', 'url' => ['ventas/ventaslogistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ventas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
