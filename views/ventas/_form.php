<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Clientes;
use app\models\Productos;

 

/* @var $this yii\web\View */
/* @var $model app\models\Ventas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ventas-form">

    <?php $form = ActiveForm::begin(); ?>
        
    <?= $form->field($model, 'referencia_interna_venta', ['template' => '{label}
<span title="Referencia impuesta. Cumple el siguiente formato:
AÑO(NºORDEN) 
Ejemplo: 202102" class="glyphicon glyphicon-info-sign"></span> *{input}'])->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'concepto_venta', ['template' => '{label}
<span class="glyphicon glyphicon-info-sign"></span> *{input}'])->textInput(['maxlength' => true]) ?>

    <?php $clientes=Clientes::find()->where('baja=0')->all();
$dataclientes=ArrayHelper::map($clientes, 'codigo_cliente', 'nombre_cliente');
echo $form->field($model, 'cod_cliente', [
    "template" => "<label> Cliente </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataclientes, ['prompt'=> 'ELIGE UN CLIENTE']) ?>

<?php $productos=Productos::find()->all();
$dataproductos=ArrayHelper::map($productos, 'codigo_producto', 'concepto_producto');
echo $form->field($model, 'cod_producto',  [
    "template" => "<label> Producto </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PRODUCTO']) ?>

    <?= $form->field($model, 'cantidad', ['template' => '{label}
<span class="glyphicon glyphicon-info-sign"></span> *{input}'])->textInput() ?>

    <?= $form->field($model, 'base_imponible_unitaria_venta', ['template' => '{label}
<span title="Los decimales se deberán expresar con puntos, no comas.
Ejemplo: 25,3=>25.3"class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

   <?= $form->field($model, 'fecha_de_salida', ['template' => '{label}
<span title="Fecha de salida de los productos del inventario."
class="glyphicon glyphicon-info-sign"></span>{input}'])->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>
  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
