<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Intermediarios;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\TlfsIntermediarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tlfs-intermediarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $intermediarios= Intermediarios::find()->where('baja=0')->all();
$dataintermediarios=ArrayHelper::map($intermediarios, 'codigo_intermediario', 'nombre_intermediario');
echo $form->field($model, 'codigo_intermediario', [
    "template" => "<label> Intermediario </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataintermediarios, ['prompt'=> 'ELIGE UN INTERMEDIARIO'])  ?>

    <?= $form->field($model, 'tlf_intermediario')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
