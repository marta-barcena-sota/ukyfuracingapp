<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Pedidos;

/* @var $this yii\web\View */
/* @var $model app\models\Documentaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="documentaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $pedidos=Pedidos::find()-> where('baja=0')->all();
$datapedidos=ArrayHelper::map($pedidos, 'codigo_pedido', 'tracking_pedido');
echo $form->field($model, 'codigo_pedido')->dropDownList($datapedidos, ['prompt'=> 'ELIGE UN PEDIDO']) ?>

    <?= $form->field($model, 'documento_de_transporte')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'justificante_de_pago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_de_factura')->textInput() ?>

    <?= $form->field($model, 'fecha_factura')->textInput() ?>

    <?= $form->field($model, 'archivo_factura')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'albaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pedido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'documento_importacion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>