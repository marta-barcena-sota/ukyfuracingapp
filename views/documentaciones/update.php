<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Documentaciones */

$this->title = 'Update Documentaciones: ' . $model->codigo_documentacion;
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = ['label' => 'DOCUMENTACIONES', 'url' => ['documentaciones/docspedidos']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
