<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mejoras */

$this->title = 'Create Mejoras';
$this->params['breadcrumbs'][] = ['label' => 'Mejoras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mejoras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
