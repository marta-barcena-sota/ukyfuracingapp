<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Empleados;

/* @var $this yii\web\View */
/* @var $model app\models\Mejoras */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mejoras-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $empleados=Empleados::find()-> where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado', [
    "template" => "<label> Empleado </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

    <?= $form->field($model, 'descripcion_mejora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carpeta_mejora')->textInput(['maxlength' => true]) ?>
    
       <?= $form->field($model, 'fecha')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
