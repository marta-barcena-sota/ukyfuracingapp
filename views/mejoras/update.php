<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mejoras */

$this->title = 'Update Mejoras: ' . $model->codigo_mejora;
$this->params['breadcrumbs'][] = ['label' => 'Mejoras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_mejora, 'url' => ['view', 'id' => $model->codigo_mejora]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mejoras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
