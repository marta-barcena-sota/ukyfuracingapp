<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IntermediariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Intermediarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intermediarios-index">

   <br><!-- comment -->
        <br><!-- comment -->
          <h1><?= Html::encode($this->title) ?></h1>
  
                  <br><!-- comment -->
         
             <?= Html::a('NUEVO INTERMEDIARIO', ['intermediarios/create'], ['class' => 'btn btn-marta']) ?>

         <br><!-- comment -->
               <br><!-- comment -->
    


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_intermediario',
//            'eori_intermediario',
//            'cif_intermediario',
            'nombre_intermediario',
//            'tipo_empresa_intermediario',
            //'sociedad_empresa_intermediario',
            //'baja',

            ['class' => 'yii\grid\ActionColumn'],
        ],
//        'options' => [ 'style' => 'background-color: whitesmoke; color:black;' ]
    ]); ?>


</div>
