<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Intermediarios */

$this->title = 'ALTA DE INTERMEDIARIO';
$this->params['breadcrumbs'][] = ['label' => 'INTERMEDIARIOS', 'url' => ['intermediarios/intermediariosenalta']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intermediarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
