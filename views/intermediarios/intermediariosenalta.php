<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = 'INTERMEDIARIOS';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="body-content">
    <br>
    <br><!-- comment -->
    
    <h1>INTERMEDIARIOS</h1>
    
    <div class="row">
        <div class="col-sm-6">
             <p>
        <?= Html::a('Dar de alta un nuevo intermediario', ['create'], ['class' => 'btn btn-marta']) ?>
                   <?= Html::a('AÑADIR ENLACE A INTERMEDIARIO', ['enlaces-intermediarios/create'], ['class' => 'btn btn-marta']) ?>
                  <?= Html::a('AÑADIR COMENTARIO A INTERMEDIARIO', ['comentarios/createintermediarios'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['intermediarios/intermediariosenbaja'], ['class' => 'btn btn-marta']) ?>
    </p>  
        </div>
       
        
    </div>
    
     <br>
    <br><!-- comment -->
   
</div>

<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_intermediario',
         'nombre_intermediario',
       
         ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_intermediario'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_intermediario'];

        	return $url;

    	}

	}]
         ],
      
        ]);?>