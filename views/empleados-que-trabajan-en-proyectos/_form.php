<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Empleados;
use app\models\Proyectos;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\EmpleadosQueTrabajanEnProyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleados-que-trabajan-en-proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $proyectos=Proyectos::find()-> where('baja=0')->all();
$dataproyectos=ArrayHelper::map($proyectos, 'codigo_proyecto', 'nombre_proyecto');
echo $form->field($model, 'codigo_proyecto', [
    "template" => "<label> Proyecto </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataproyectos, ['prompt'=> 'ELIGE UN PROYECTO']) ?>

    <?php $empleados=Empleados::find()-> where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado', [
    "template" => "<label> Empleado </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

   <?= $form->field($model, 'responsable', ['template' => '{label}
*{input}'])->dropDownList(
            [0=>'NO',
                1=> 'SÍ',
            ]
            , ['prompt'=> '¿ES EL RESPONSABLE DEL PROYECTO?']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
