<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Clientes;


/* @var $this yii\web\View */
/* @var $model app\models\Proyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $clientes=Clientes::find()-> where('baja=0')->all();
$dataclientes=ArrayHelper::map($clientes, 'codigo_cliente', 'nombre_cliente');
echo $form->field($model, 'codigo_cliente', [
    "template" => "<label> Cliente </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataclientes, ['prompt'=> 'ELIGE UN CLIENTE']) ?>

    <?= $form->field($model, 'referencia_interna_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_proyecto', ['template' => '{label}
*{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'referencia_cad_proyecto')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'carpeta_proyecto')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'gantt_proyecto')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'fecha_inicio_proyecto', ['template' => '{label}
<span title="Fecha inicio del proyecto."
class="glyphicon glyphicon-info-sign"></span>{input}'])->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

       <?= $form->field($model, 'fecha_fin_proyecto', ['template' => '{label}
<span title="Fecha fin del proyecto."
class="glyphicon glyphicon-info-sign"></span>{input}'])->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'tipo_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado_proyecto')->dropdownList([
        'NUEVA ORDEN'=>'NUEVA ORDEN', 
        'PDTE PRESUPUESTO'=>'PDTE PRESUPUESTO',
       'EN COLA'=>'EN CURSO',
        'REVISION'=>'REVISION',
        'POSTPROCESADO'=>'POSTPROCESADO',
        'FINALIZADO'=>'FINALIZADO',
    ],
    ['prompt'=>'Seleccione un estado']
) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
