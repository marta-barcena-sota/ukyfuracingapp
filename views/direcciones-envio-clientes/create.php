<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionesEnvioClientes */

$this->title = 'Create Direcciones Envio Clientes';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'CLIENTES', 'url' => ['clientes/clienteslogistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direcciones-envio-clientes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
