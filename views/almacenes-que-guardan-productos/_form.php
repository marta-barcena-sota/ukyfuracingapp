<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Almacenes;
use yii\helpers\ArrayHelper;
use app\models\Productos;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\AlmacenesQueGuardanProductos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="almacenes-que-guardan-productos-form">

    <?php $form = ActiveForm::begin(); ?>

      <?php $almacenes= Almacenes::find()->where('baja=0')->all();
$dataalmacenes=ArrayHelper::map($almacenes, 'codigo_almacen', 'denominacion_almacen');
echo $form->field($model, 'codigo_almacen')->dropDownList($dataalmacenes, ['prompt'=> 'ELIGE UN ALMACÉN'])?>

    <?php $productos= Productos::find()->where('baja=0')->all();
$dataproductos=ArrayHelper::map($productos, 'codigo_producto', 'concepto_producto');
echo $form->field($model, 'codigo_producto')->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PRODUCTO'])?>

  <?= $form->field($model, 'fecha_ingreso_producto')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
