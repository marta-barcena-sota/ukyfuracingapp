<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionesFacturacionClientes */

$this->title = 'Create Direcciones Facturacion Clientes';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'CLIENTES', 'url' => ['clientes/clienteslogistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direcciones-facturacion-clientes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
