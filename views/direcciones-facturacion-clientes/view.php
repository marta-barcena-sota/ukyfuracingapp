<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionesFacturacionClientes */

$this->title = $model->codigo_direccion_facturacion;
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'CLIENTES', 'url' => ['clientes/clienteslogistica']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="direcciones-facturacion-clientes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_direccion_facturacion], ['class' => 'btn btn-marta']) ?>
<!--        = Html::a('Delete', ['delete', 'id' => $model->codigo_direccion_facturacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_direccion_facturacion',
            'codigo_cliente',
            'direccion_facturacion',
            'baja',
        ],
    ]) ?>

</div>
