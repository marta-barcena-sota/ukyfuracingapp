<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Clientes;

/* @var $this yii\web\View */
/* @var $model app\models\DireccionesFacturacionClientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="direcciones-facturacion-clientes-form">

    <?php $form = ActiveForm::begin(); ?>
    
<?php $clientes=Clientes::find()->where('baja=0')->all();
$dataclientes=ArrayHelper::map($clientes, 'codigo_cliente', 'nombre_cliente');
echo $form->field($model, 'codigo_cliente', [
    "template" => "<label> Cliente </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataclientes, ['prompt'=> 'ELIGE UN CLIENTE']) ?>

    <?= $form->field($model, 'direccion_facturacion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
