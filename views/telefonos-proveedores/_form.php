<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Proveedores;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\TelefonosProveedores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telefonos-proveedores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor', [
    "template" => "<label> PROVEEDOR </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])  ?>

    <?= $form->field($model, 'tlf_proveedor')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
