<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonosProveedores */

$this->title = 'Update Telefonos Proveedores: ' . $model->codigo_tlf_proveedor;
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = ['label' => 'TLFS PROVEEDORES', 'url' => ['telefonos-proveedores/proveedorestlfs']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonos-proveedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
