<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        

        .navbar-marta{
          background-color:#0FC6C0;
           
        }
        .elementoslayout{
         color: #EFF5F9;
          font-weight:bold;
        }
       .elementoslayout:hover{
         color: #0FC6C0 !important;
         background-color:#EFF5F9 !important; 
        }
        
 
       .active .elementoslayout{
              color: #0FC6C0 !important;
         background-color:#EFF5F9 !important; 
          font-weight:bold;
            
        }
        .navbar-brand {

            padding: 3px;

        }

        .navbar-brand img {

            height: 45px;
            width: 45px;

        }
        
 

    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([

        'brandLabel' => Html::img('@web/images/logo1.png', ['alt'=>Yii::$app->name]),
//        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-marta navbar-fixed-top',
        ],
    ]);
    
    echo Nav::widget([
        /*SI CAMBIAS EL NAVBAR-NAV POR OTRA CLASE, LOS ITEMS
         * SE DESESTRUCTURAN EN LA VISTA
         */
//        PARA METER EL MENÚ DESPLEGABLE SÓLO HABRÍA QUE AÑADIR ESTO AL APARTADO
//        DE AGENDA
//        ,'items' => [
//                ['label' => 'Clientes', 'url' => '/'],
//                ['label' => 'Proveedores', 'url' => '#'],
//                ['label' => 'Empleados', 'url' => '#'],
//                ['label' => 'Intermediarios', 'url' => '#'],
//            ]
//        'options' => ['class' => 'navbar-nav navbar-left'],
//        'encodeLabels' => false,
//        'items' => [
//            ['label' => 'HOME'
//                , 'url' => ['/site/index'], 'linkOptions'=>['class'=>'elementoslayout']],
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login'], 'linkOptions'=>['class'=>'elementoslayout ']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
        ],
        
    );
    NavBar::end();
    ?>

    <div class="container" >
      <?= 
   Breadcrumbs::widget([
      'homeLink' => [ 
                      'label' => Yii::t('yii', 'Inicio'),
                      'url' => Yii::$app->homeUrl,
                 ],
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
   ]) 
?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; MBS DEVELOPMENT <?= date('Y') ?></p>

<!--        <p class="pull-right"><= Yii::powered() ?></p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
