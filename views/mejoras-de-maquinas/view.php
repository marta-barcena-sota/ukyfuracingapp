<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasDeMaquinas */

$this->title = $model->codigo_aplicacion;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'MÁQUINAS', 'url' => ['maquinas/maquinasenalta']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mejoras-de-maquinas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_aplicacion], ['class' => 'btn btn-marta']) ?>
<!--         Html::a('Delete', ['delete', 'id' => $model->codigo_aplicacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_aplicacion',
            'codigo_maquina',
            'codigo_mejora',
            'baja',
        ],
    ]) ?>

</div>
