<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasDeMaquinas */

$this->title = 'Update Mejoras De Maquinas: ' . $model->codigo_aplicacion;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'MÁQUINAS', 'url' => ['maquinas/maquinasenalta']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mejoras-de-maquinas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
