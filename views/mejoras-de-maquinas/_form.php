<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Maquinas;
use app\models\Mejoras;

/* @var $this yii\web\View */
/* @var $model app\models\MejorasDeMaquinas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mejoras-de-maquinas-form">

    <?php $form = ActiveForm::begin(); ?>
    
<?php $maquinas= Maquinas::find()->where('baja=0')->all();
$datamaquinas=ArrayHelper::map($maquinas, 'codigo_maquina', 'nombre_maquina');
echo $form->field($model, 'codigo_maquina')->dropDownList($datamaquinas, ['prompt'=> 'ELIGE UNA MÁQUINA'])?>

    <?php $mejoras= Mejoras::find()->where('baja=0')->all();
$datamejoras=ArrayHelper::map($mejoras, 'codigo_mejora', 'descripcion_mejora');
echo $form->field($model, 'codigo_mejora')->dropDownList($datamejoras, ['prompt'=> 'ELIGE UNA MEJORA'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
