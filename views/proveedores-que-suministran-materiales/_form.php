<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;
use app\models\Materiales;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranMateriales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-suministran-materiales-form">

    <?php $form = ActiveForm::begin(); ?>

       <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])?>

       <?php $materiales= Materiales::find()->where('baja=0')->all();
$datamateriales=ArrayHelper::map($materiales, 'codigo_material', 'familia_material');
echo $form->field($model, 'codigo_material')->dropDownList($datamateriales, ['prompt'=> 'ELIGE UN MATERIALES'])?>

    <?= $form->field($model, 'base_imponible_unitaria')->textInput() ?>

    <?= $form->field($model, 'iva')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
