<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranMateriales */

$this->title = 'Create Proveedores Que Suministran Materiales';
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'PROVEEDORES DE MATERIALES', 'url' => ['proveedores/provmateriales']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-materiales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
