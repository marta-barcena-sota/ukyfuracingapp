<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranMateriales */

$this->title = $model->codigo_suministro;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'PROVEEDORES DE MATERIALES', 'url' => ['proveedores/provmateriales']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="proveedores-que-suministran-materiales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_suministro], ['class' => 'btn btn-marta']) ?>
<!--       Html::a('Delete', ['delete', 'id' => $model->codigo_suministro], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_suministro',
            'codigo_proveedor',
            'codigo_material',
            'base_imponible_unitaria',
            'iva',
            'baja',
        ],
    ]) ?>

</div>
