<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranMateriales */

$this->title = 'Update Proveedores Que Suministran Materiales: ' . $model->codigo_suministro;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'PROVEEDORES DE MATERIALES', 'url' => ['proveedores/provmateriales']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-que-suministran-materiales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
