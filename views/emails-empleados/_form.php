<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Empleados;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsEmpleados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emails-empleados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $empleados=Empleados::find()-> where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado', [
    "template" => "<label> Empleado </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

    <?= $form->field($model, 'email_empleado', ['template' => '{label}
*{input}'])->textInput(['maxlength' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
