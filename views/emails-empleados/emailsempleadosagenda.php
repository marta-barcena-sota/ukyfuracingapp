<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = 'EMAILS EMPLEADOS';

$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="body-content">
    <br><!-- comment -->
    <br><!-- comment -->
    <h1>EMAILS EMPLEADOS</h1>
     <br><!-- comment -->
    <br><!-- comment -->
    <div class="row">
        <div class="col-sm-6">
             <p>
        <?= Html::a('Dar de alta un nuevo email', ['create'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['emails-empleados/emailsempleadospapelera'], ['class' => 'btn btn-marta']) ?>
    </p>  
        </div>
       
        
    </div>
    <br><!-- comment -->
    <br><!-- comment -->
</div>

<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
         'email_empleado',
        
                             ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_email'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_email'];

        	return $url;

    	}

	}]
         ],
      
        ]);?>
