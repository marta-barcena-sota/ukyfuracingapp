<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsEmpleados */

$this->title = 'ALTA EMAIL EMPLEADO';
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = ['label' => 'EMAILS EMPLEADOS', 'url' => ['emails-empleados/empleadosemails']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emails-empleados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
