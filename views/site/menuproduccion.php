<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'PRODUCCIÓN';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">

    <div class="body-content">
        
        <div class="row">
            
            <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/bdfab.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('BD FABRICACIÓN', ['ordenes-de-fabricacion/ordenesproduccion'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>
            
             <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/proyectos.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('PROYECTOS', ['proyectos/proyectosproduccion'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>

 <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/maquinas.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('MAQUINAS', ['maquinas/maquinasenalta'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>            
        </div>
        
              <div class="row">
            
            <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/proveedoresdemateriales.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('PROV MATERIALES', ['proveedores/provmateriales'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>
            
             <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/proveedoresdefabricacion.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('PROV FABRICACIÓN', ['proveedores/provfabricacionproduccion'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>
                  
                   <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/proveedoresdepostprocesados.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('PROV POSTPROCESADOS', ['proveedores/provpostprocesadosproduccion'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>

           
        </div>
                    <div class="row">
            
           
            
             <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/materiales.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('MATERIALES', ['materiales/materialesproduccion'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>
               

                        
     
        </div>
            </div>
        
    </div>
    
   
