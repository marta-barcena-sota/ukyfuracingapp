<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Equipo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    
    <div class="body-content">

        
         <div class="colocacionbotonopciones">
           <div class="row">
        <div class="col-sm-6">
             <p>
                  <?= Html::a('Ver todos los empleados', ['empleados/index'], ['class' => 'btn btn-marta']) ?>
                      
    </p>  
        </div>
                    </div> 
        </div>
        <div class="opciones">
             
            <div class="row">
            
            <div class="col-sm-6">
           
                    <div class="caption">
                       <?= Html::img('@web/images/empleadosenalta.jpg', ['alt' => 'My logo']) ?>
                         <?=Html::a ('Empleados en alta', ['empleados/empleadosenalta'] ,['class'=> 'bottom-right'])?>
                    </div>
                    
                
                
            </div>
             <div class="col-sm-6">
                 
                    <div class="caption">
                        <?= Html::img('@web/images/empleadosenbaja.jpg', ['alt' => 'My logo']) ?>
                           <?=Html::a ('Empleados en baja', ['empleados/empleadosenbaja'] ,['class'=> 'bottom-right'])?>
                        
                  
                    
                </div>
                
            </div>
            
        </div> 
        </div>
       
        
           
    </div>
</div>
