<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'UKYFU RACING';

?>
<div class="site-index">

    
    <div class="body-content">
     

<div class="row">
    <div class="col-sm-2">
        
    </div>
    <div class="col-sm-8">
        <div class="imagen">
       <?= Html::img('@web/images/titulohome.png', ['class' => 'titulo-index']) ?>       
        </div>
    
    </div>
     <div class="col-sm-2">
        
    </div>  
</div>
 
        
       
        
         <div class="row">
             <div class="col-sm-3">
                 
             </div>
            <div class="col-sm-3">

                    <div class="caption">
                       
                        <?= Html::a('LOGÍSTICA', ['site/menulogistica']) ?>
                       
                    </div>

            </div>
            
             <div class="col-sm-3">
                  <div class="caption">
                        
                     <?= Html::a('PRODUCCIÓN', ['site/menuproduccion']) ?>
                    </div>    
            </div>
                <div class="col-sm-3">
                 
             </div>
             
             
        </div>
        
        <br>
        <br>
        
        <div class="row">
             <div class="col-sm-4">
                 <div class="caption">
                      <?= Html::a('INTERMEDIARIOS', ['intermediarios/index']) ?>  
                 </div>
               
             </div>
            <div class="col-sm-4">

                    <div class="caption">
                       
                        <?= Html::a('PROVEEDORES', ['proveedores/index']) ?>
                       
                    </div>

            </div>
            
             <div class="col-sm-4">
                  <div class="caption">
                        
                     <?= Html::a('EMPLEADOS', ['empleados/index']) ?>
                    </div>    
            </div>
   
             
             
        </div>
       
             
        </div>
           
    </div>
   
    
