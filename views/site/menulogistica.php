<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'LOGÍSTICA';
$this->params['breadcrumbs'][] = $this->title;

?>


    <div class="body-content">
        
        <div class="row">
            
            <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="menu-logistica">
                       
                        <?= Html::img('@web/images/bdgral.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('BD GRAL', ['site/infobdgral']) ?>
                      
                    </div>
                    
                </div>

            </div>
            
             <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/provproductos.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('PROV. PROD.', ['proveedores/proveedoresproductoslogistica'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>

 <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/provtransporte.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('PROV. TRANS.', ['proveedores/proveedorestransportelogistica'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>            
        </div>
        
              <div class="row">
            
            <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/clientes.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('CLIENTES', ['clientes/clienteslogistica'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>
            
             <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/cambioalmacen.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('ALMACENES', ['almacenes/almaceneslogistica'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>
                  
                   <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/inventario.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('INVENTARIO', ['productos/inventariologistica'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>

           
        </div>
                    <div class="row">
            
           
            
             <div class="col-sm-4">
                <div class="thumbnail">
                   
                      <div class="caption">
                       
                        <?= Html::img('@web/images/ventas.png', ['alt' => 'My logo']) ?>
                         <?= Html::a('VENTAS', ['ventas/ventaslogistica'], ['class' => 'menu-logistica']) ?>
                      
                    </div>
                    
                </div>

            </div>

                       
     
        </div>
            </div>
        
  
   
