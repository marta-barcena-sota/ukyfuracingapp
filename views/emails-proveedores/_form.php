<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;

/* @var $this yii\web\View */
/* @var $model app\models\EmailsProveedores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emails-proveedores-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])?>

    <?= $form->field($model, 'email_proveedor')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
