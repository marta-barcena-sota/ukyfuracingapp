<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Almacenes */

$this->title = 'ALTA DE ALMACEN';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'ALMACENES', 'url' => ['almacenes/almaceneslogistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="almacenes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
