<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;
use app\models\Postprocesados;
use app\models\OrdenesDeFabricacion;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranPostprocesadosAOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-suministran-postprocesados-aordenes-form">

    <?php $form = ActiveForm::begin(); ?>

   <?php $postprocesados= Postprocesados::find()->where('baja=0')->all();
$datapostprocesados=ArrayHelper::map($postprocesados, 'codigo_postprocesado', 'descripcion_postprocesado');
echo $form->field($model, 'codigo_postprocesado')->dropDownList($datapostprocesados, ['prompt'=> 'ELIGE UN POSTPROCESADO'])?>

   <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])?>

  <?php $ordenesdefabricacion= OrdenesDeFabricacion::find()->where('baja=0')->all();
$dataordenesdefabricacion=ArrayHelper::map($ordenesdefabricacion, 'codigo_orden_de_fabricacion', 'nombre_orden_de_fabricacion');
echo $form->field($model, 'codigo_orden')->dropDownList($dataordenesdefabricacion, ['prompt'=> 'ELIGE UNA ORDEN DE FABRICACION'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
