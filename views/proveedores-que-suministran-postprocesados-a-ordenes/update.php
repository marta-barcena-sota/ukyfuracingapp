<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueSuministranPostprocesadosAOrdenes */

$this->title = 'Update Proveedores Que Suministran Postprocesados A Ordenes: ' . $model->codigo_suministro;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'ÓRDENES DE FABRICACIÓN', 'url' => ['ordenes-de-fabricacion/ordenesproduccion']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="proveedores-que-suministran-postprocesados-aordenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
