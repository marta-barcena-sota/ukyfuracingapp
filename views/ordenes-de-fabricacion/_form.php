<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Empleados;
use app\models\Proyectos;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\OrdenesDeFabricacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ordenes-de-fabricacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $empleados=Empleados::find()-> where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado', [
    "template" => "<label> Empleado * </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

    <?php $proyectos=Proyectos::find()-> where('baja=0')->all();
$dataproyectos=ArrayHelper::map($proyectos, 'codigo_proyecto', 'nombre_proyecto');
echo $form->field($model, 'codigo_proyecto', [
    "template" => "<label> Proyecto * </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataproyectos, ['prompt'=> 'ELIGE UN PROYECTO']) ?>

    <?= $form->field($model, 'nombre_orden_de_fabricacion', ['template' => '{label}
*{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado', ['template' => '{label}
*{input}'])->dropdownList([
        'NUEVA ORDEN'=>'NUEVA ORDEN', 
        'PDTE PRESUPUESTO'=>'PDTE PRESUPUESTO',
       'EN COLA'=>'EN CURSO',
        'REVISION'=>'REVISION',
        'POSTPROCESADO'=>'POSTPROCESADO',
        'FINALIZADO'=>'FINALIZADO',
    ],
    ['prompt'=>'Seleccione un estado']
) ?>

    <?= $form->field($model, 'fecha_inicio_orden_de_fabricacion', ['template' => '{label}
<span title="Fecha de inicio de la orden de fabricacion." class="glyphicon glyphicon-info-sign"></span>{input}'])->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'fecha_fin_orden_de_fabricacion', ['template' => '{label}
<span title="Fecha de la finalizacion de la orden de fabricacion." class="glyphicon glyphicon-info-sign"></span>{input}'])->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'tipo_orden_de_fabricacion', ['template' => '{label}
*{input}'])->dropDownList(
            ['A'=>'AJENA',
                'P'=> 'PROPIA',
            ]
            , ['prompt'=> 'ELIGE UN TIPO DE ORDEN']) ?> 

    <?= $form->field($model, 'fecha_estimada_fin_orden_fabricacion', ['template' => '{label}
<span title="Fecha estimada de la finalizacion de la orden de fabricacion." class="glyphicon glyphicon-info-sign"></span>{input}'])->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'urgencia_orden_fabricacion')->dropDownList(
            ['PRIORIDAD ALTA'=>'PRIORIDAD ALTA',
                'PRIORIDAD MEDIA'=> 'PRIORIDAD MEDIA',
                'PRIORIDAD BAJA'=>'PRIORIDAD BAJA',
                
            ]
            , ['prompt'=> 'ELIGE UNA URGENCIA'])  ?>

    <?= $form->field($model, 'estimacion_coste_material', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

    <?= $form->field($model, 'coste_fabricacion', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

    <?= $form->field($model, 'coste_transporte', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

    <?= $form->field($model, 'coste_postprocesado', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

    <?= $form->field($model, 'coste_maquina', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

    <?= $form->field($model, 'coste_mano_de_obra', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

    <?= $form->field($model, 'coste_material', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

    <?= $form->field($model, 'coste_total', ['template' => '{label}
<span title="En caso de introducirse una cantidad decimal, el separador
ha de ser un punto.

Ejemplo: 25.2" class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput() ?>

  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
