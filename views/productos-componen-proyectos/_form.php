<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Productos;
use app\models\Proyectos;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosComponenProyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-componen-proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php $proyectos= Proyectos::find()->where('baja=0')->all();
$dataproyectos=ArrayHelper::map($proyectos, 'codigo_proyecto', 'nombre_proyecto');
echo $form->field($model, 'codigo_proyecto')->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PROYECTO'])?>

    <?php $productos= Productos::find()->where('baja=0')->all();
$dataproductos=ArrayHelper::map($productos, 'codigo_producto', 'concepto_producto');
echo $form->field($model, 'codigo_producto')->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PRODUCTO'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
