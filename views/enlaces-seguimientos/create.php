<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EnlacesSeguimientos */

$this->title = 'Create Enlaces Seguimientos';
$this->params['breadcrumbs'][] = ['label' => 'Enlaces Seguimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enlaces-seguimientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
