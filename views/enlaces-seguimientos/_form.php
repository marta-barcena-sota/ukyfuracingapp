<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Seguimientos;

/* @var $this yii\web\View */
/* @var $model app\models\EnlacesSeguimientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enlaces-seguimientos-form">

    <?php $form = ActiveForm::begin(); ?>

         <?php $seguimientos= Seguimientos::find()->where('baja=0')->all();
$dataseguimientos=ArrayHelper::map($seguimientos, 'codigo_seguimiento', 'concepto_seguimiento');
echo $form->field($model, 'codigo_seguimiento')->dropDownList($dataseguimientos, ['prompt'=> 'ELIGE UN PROYECTO'])?>

    <?= $form->field($model, 'enlace_seguimiento')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
