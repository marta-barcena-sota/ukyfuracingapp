<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Seguimientos */

$this->title = 'ALTA DE SEGUIMIENTO PARA PEDIDO';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = ['label' => 'PEDIDOS', 'url' => ['pedidos/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seguimientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formpedido', [
        'model' => $model,
    ]) ?>

</div>
