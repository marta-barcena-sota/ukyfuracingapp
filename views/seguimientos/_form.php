<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Empleados;
use app\models\Pedidos;
use app\models\OrdenesDeFabricacion;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Seguimientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seguimientos-form">

    <?php $form = ActiveForm::begin(); ?>

          <?php $pedidos= Pedidos::find()->where('baja=0')->all();
$datapedidos=ArrayHelper::map($pedidos, 'codigo_pedido', 'referencia_interna');
echo $form->field($model, 'codigo_pedido')->dropDownList($datapedidos, ['prompt'=> 'ELIGE UN PEDIDO'])?>

    <?= $form->field($model, 'concepto_seguimiento')->textInput(['maxlength' => true]) ?>

    <?php $ordenesdefabricacion= OrdenesDeFabricacion::find()->where('baja=0')->all();
$dataordenesdefabricacion=ArrayHelper::map($ordenesdefabricacion, 'codigo_orden_de_fabricacion', 'nombre_orden_de_fabricacion');
echo $form->field($model, 'codigo_orden_de_fabricacion')->dropDownList($dataordenesdefabricacion, ['prompt'=> 'ELIGE UNA ORDEN DE FABRICACION']) ?>

  <?php $empleados= Empleados::find()->where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado')->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

     <?= $form->field($model, 'hora_seguimiento')->widget(\pheme\jui\DateTimePicker::className(), ['timeOnly' => true]) ?> 

    <?= $form->field($model, 'fecha_de_realizacion_seguimiento')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?> 

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
