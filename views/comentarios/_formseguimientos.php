<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Empleados;
use app\models\Seguimientos;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Comentarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comentarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $empleados=Empleados::find()-> where('baja=0')->all();
$dataempleados=ArrayHelper::map($empleados, 'codigo_empleado', 'nombre_empleado');
echo $form->field($model, 'codigo_empleado', [
    "template" => "<label> Empleado </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataempleados, ['prompt'=> 'ELIGE UN EMPLEADO']) ?>

      <?php $seguimientos=Seguimientos::find()-> where('baja=0')->all();
$dataseguimientos=ArrayHelper::map($seguimientos, 'codigo_seguimiento', 'concepto_seguimiento');
echo $form->field($model, 'codigo_seguimiento', [
    "template" => "<label> Seguimientos </label>\n{input}\n{hint}\n{error}"
])->dropDownList($dataseguimientos, ['prompt'=> 'ELIGE UN SEGUIMIENTO']) ?>

    <?= $form->field($model, 'concepto_comentario')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'hora_comentario')->widget(\pheme\jui\DateTimePicker::className(), ['timeOnly' => true]) ?> 

    <?= $form->field($model, 'fecha_comentario')->widget(DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?> 



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
