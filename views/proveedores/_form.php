<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cif_proveedor', ['template' => '{label}
<span title="El término CIF significa: Código de Identificación Fiscal.
Compuesto por 9 caracteres, responde al siguiente formato:
X99999999
La letra que incluye el código representa el tipo de sociedad a la que corresponde. 
Por ejemplo, “A” si es sociedad anónima, o “B”, si son sociedades de responsabilidad limitada. 
Además, de los dígitos que incluye, los dos primeros hacen referencia a la provincia o 
comunidad autónoma en la que se encuentra constituida dicha 
empresa o personalidad jurídica. " class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eori_proveedor', ['template' => '{label}
<span title="El EORI es un número identificativo que sirve a las empresas para que puedan realizar sus operaciones aduaneras.
CUMPLE EL SIGUIENTE FORMATO.
ABX99999999
Siendo AB el código correspondiente al país al que pertenece, seguido del CIF de la empresa." class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_proveedor', ['template' => '{label}
*{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_de_empresa_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sociedad_empresa_proveedor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pais_proveedor')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'tipo_proveedor', ['template' => '{label}
*{input}'])->dropdownList([
        'MATERIALES'=>'MATERIALES', 
        'FABRICACION'=>'FABRICACION',
       'TRANSPORTE'=>'TRANSPORTE',
        'POSTPROCESADOS'=>'POSTPROCESADOS',
        'PRODUCTOS'=>'PRODUCTOS'
    ],
    ['prompt'=>'Seleccione un tipo']
)?>
    

<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
