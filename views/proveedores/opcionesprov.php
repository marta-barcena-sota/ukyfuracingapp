<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Agenda: Proveedores';
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    
    <div class="body-content">

        <div class="colocacionbotonopciones">
           <div class="row">
        <div class="col-sm-6">
             <p>
                  <?= Html::a('Ver todos los proveedores', ['proveedores/index'], ['class' => 'btn btn-marta']) ?>
                      
    </p>  
        </div>
                    </div> 
        </div>
        <div class="prueba">
             
            <div class="opciones">
            
            <div class="col-sm-6">
           
                    <div class="caption">
                       <?= Html::img('@web/images/proveedoresenalta.jpg', ['alt' => 'My logo']) ?>
                         <?=Html::a ('Proveedores en alta', ['proveedores/proveedoresenalta'] ,['class'=> 'bottom-right'])?>
                    </div>
                    
                
                
            </div>
             <div class="col-sm-6">
                 
                    <div class="caption">
                        <?= Html::img('@web/images/proveedoresenbaja.jpg', ['alt' => 'My logo']) ?>
                           <?=Html::a ('Proveedores en baja', ['proveedores/proveedoresenbaja'] ,['class'=> 'bottom-right'])?>
                        
                  
                    
                </div>
                
            </div>
            
        </div> 
        </div>
       
           
    </div>
</div>