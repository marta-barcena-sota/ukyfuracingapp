<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-index">

      <br><!-- comment -->
        <br><!-- comment -->
          <h1><?= Html::encode($this->title) ?></h1>
  
                  <br><!-- comment -->
         
             <?= Html::a('NUEVO PROVEEDOR', ['proveedores/create'], ['class' => 'btn btn-marta']) ?>
                   

         <br><!-- comment -->
               <br><!-- comment -->
    

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_proveedor',
//            'cif_proveedor',
//            'eori_proveedor',
            'nombre_proveedor',
//            'tipo_de_empresa_proveedor',
            //'sociedad_empresa_proveedor',
            //'pais_proveedor',
            //'tipo_proveedor',
            //'baja',

            ['class' => 'yii\grid\ActionColumn'],
        ],
//        'options' => [ 'class' => 'grid-view tr' ]
        
    ]); ?>


</div>
