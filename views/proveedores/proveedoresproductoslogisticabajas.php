<?php

/* @var $this yii\web\View */
/* @var $model app\models\Proveedores */

use yii\helpers\Html;

$this->title = 'PROVEEDORES DE PRODUCTOS: PAPELERA';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'PROVEEDORES DE PRODUCTOS', 'url' => ['proveedores/proveedoresproductoslogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
        <br>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/provproductos.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>PROVEEDORES DE PRODUCTOS: PAPELERA</h1>
                    
                </div>
                
            </div>
        </div>
       <br>
        <br>
           
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_proveedor',
         'nombre_proveedor',
         'cif_proveedor',
         'tipo_de_empresa_proveedor',
         'pais_proveedor',
       ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {alta}, {delete}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'alta' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-ok"></span>',$url, [
                            'title' => Yii::t('app', 'lead-alta'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            },
                    'delete' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url, [
                            'title' => Yii::t('app', 'lead-delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_proveedor'];

        	return $url;

    	}
        
         if ($action === 'alta') {

        	$url = './alta?id='.$model['codigo_proveedor'];

        	return $url;

    	}
        if ($action === 'delete') {

        	$url = './delete?id='.$model['codigo_proveedor'];

        	return $url;

    	}


	}]
         ],
        ]);?>

