<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cif_cliente', ['template' => '{label}
<span title="El término CIF significa: Código de Identificación Fiscal.
Compuesto por 9 caracteres, responde al siguiente formato:
X99999999
La letra que incluye el código representa el tipo de sociedad a la que corresponde. 
Por ejemplo, “A” si es sociedad anónima, o “B”, si son sociedades de responsabilidad limitada. 
Además, de los dígitos que incluye, los dos primeros hacen referencia a la provincia o 
comunidad autónoma en la que se encuentra constituida dicha 
empresa o personalidad jurídica." class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_cliente', ['template' => '{label}
*{input}'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_empresa_cliente', ['template' => '{label}
<span title="Se requiere la forma jurídica de la empresa." class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>
    
    
   <?= $form->field($model, 'sociedad_empresa_cliente')->textInput(['maxlength' => true]) ?>
       <?= $form->field($model, 'nombre_contacto_cliente')->textInput(['maxlength' => true]) ?>
       <?= $form->field($model, 'primer_apellido_contacto_cliente')->textInput(['maxlength' => true]) ?>
       <?= $form->field($model, 'segundo_apellido_contacto_cliente')->textInput(['maxlength' => true]) ?>
    
        <?= $form->field($model, 'pais_cliente', ['template' => '{label}
<span title="Se requieren la abreviatura equivalente al dominio de internet
correspondiente de cada país. Por ejemplo:
Reino Unido => UK. " class="glyphicon glyphicon-info-sign"></span>{input}'])->textInput(['maxlength' => true]) ?>
    


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
