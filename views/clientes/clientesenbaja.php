<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;

$this->title = 'Bajas de clientes';
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = ['label' => 'Agenda: Clientes', 'url' => ['clientes/clientesopciones']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    
<!--    <p class="lead"> <?= $enunciado ?> </p>-->
<!--    <div class="well">
        <?= $sql ?>
        
    </div>-->
    
</div>

<div class="body-content">
    <div class="row">
        <div class="col-sm-6">
             <p>           
                      <?= Html::a('Contactar', ['clientes/index'], ['class' => 'btn btn-marta']) ?>
    </p>  
        </div>
       
        
    </div>
   
</div>

<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
    'columns'=>$campos,
//    'options' => [ 'style' => 'background-color: whitesmoke; color:black;' ]
        ]);?>
