<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->nombre_cliente;
//$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'CLIENTES', 'url' => ['clientes/clienteslogistica']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clientes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_cliente], ['class' => 'btn btn-marta']) ?>
<!--         Html::a('Delete', ['delete', 'id' => $model->codigo_cliente], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_cliente',
            'pais_cliente',
            'cif_cliente',
            'nombre_cliente',
            'tipo_empresa_cliente',
            'sociedad_empresa_cliente',
            'nombre_contacto_cliente',
            'primer_apellido_contacto_cliente',
            'segundo_apellido_contacto_cliente',
//            'baja',
        ],
    ]) ?>

</div>
