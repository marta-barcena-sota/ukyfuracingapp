<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-index">

      <br><!-- comment -->
        <br><!-- comment -->
          <h1><?= Html::encode($this->title) ?></h1>
  
                  <br><!-- comment -->
         
             <?= Html::a('NUEVO CLIENTE', ['clientes/create'], ['class' => 'btn btn-marta']) ?>

         <br><!-- comment -->
               <br><!-- comment -->
    

 

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_cliente',
//            'pais_cliente',
//            'cif_cliente',
            'nombre_cliente',
//            'tipo_empresa_cliente',
            //'sociedad_empresa_cliente',
            //'nombre_contacto_cliente',
            //'primer_apellido_contacto_cliente',
            //'segundo_apellido_contacto_cliente',
            //'baja',

            ['class' => 'yii\grid\ActionColumn'],
        ],
//        'options' => [ 'style' => 'background-color: whitesmoke; color:black;' ]
    ]); ?>


</div>
