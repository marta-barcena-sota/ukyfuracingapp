<?php

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */


use yii\helpers\Html;

$this->title = 'CLIENTES';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
        <br>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/clientes.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>CLIENTES</h1>
                    
                </div>
                
            </div>
        </div>
        
        <br>
        <br>
        
          <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('Registrar un nuevo cliente', ['create'], ['class' => 'btn btn-marta']) ?>
        
                      <?= Html::a('Papelera', ['clientes/clienteslogisticabajas'], ['class' => 'btn btn-marta']) ?>
    </p>    
            </div>
        </div>
        
        <br>
        <br>
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
    'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//        'codigo_cliente',
         'nombre_cliente',
         'pais_cliente',
         'cif_cliente',
                ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_cliente'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_cliente'];

        	return $url;

    	}

	}]
         ],
      
        ]);?>

