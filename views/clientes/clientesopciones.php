<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Agenda: Clientes';
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['site/agenda']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    
    <div class="body-content">

        <div class="colocacionbotonopciones">
           <div class="row">
        <div class="col-sm-6">
             <p>
                  <?= Html::a('Ver todos los clientes', ['clientes/index'], ['class' => 'btn btn-marta']) ?>
                      
    </p>  
        </div>
                    </div> 
        </div>
        <div class="opciones">
             
            <div class="row">
            
            <div class="col-sm-6">
           
                    <div class="caption">
                       <?= Html::img('@web/images/clientesenalta.jpg', ['alt' => 'My logo']) ?>
                         <?=Html::a ('Clientes en alta', ['clientes/clientesenalta'] ,['class'=> 'bottom-right'])?>
                    </div>
                    
                
                
            </div>
             <div class="col-sm-6">
                 
                    <div class="caption">
                        <?= Html::img('@web/images/clientesenbaja.jpg', ['alt' => 'My logo']) ?>
                           <?=Html::a ('Clientes en baja', ['clientes/clientesenbaja'] ,['class'=> 'bottom-right'])?>
                        
                  
                    
                </div>
                
            </div>
            
        </div> 
        </div>
       
           
    </div>
</div>