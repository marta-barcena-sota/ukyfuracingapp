<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recepciones */

$this->title = 'ALTA RECEPCIÓN';
$this->params['breadcrumbs'][] = ['label' => 'LOGÍSTICA', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'BD GRAL', 'url' => ['site/infobdgral']];
$this->params['breadcrumbs'][] = ['label' => 'RECEPCIONES', 'url' => ['recepciones/recepcioneslogistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formrecepciones', [
        'model' => $model,
    ]) ?>

</div>
