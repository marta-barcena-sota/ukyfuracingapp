<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueSufrenCambios */

$this->title = 'Update Productos Que Sufren Cambios: ' . $model->codigo_traspaso;
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'INVENTARIO', 'url' => ['productos/inventariologistica']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-que-sufren-cambios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
