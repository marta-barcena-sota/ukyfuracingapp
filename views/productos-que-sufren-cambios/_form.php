<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Productos;
use app\models\Cambios;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueSufrenCambios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-que-sufren-cambios-form">

    <?php $form = ActiveForm::begin(); ?>

   <?php $productos= Productos::find()->where('baja=0')->all();
$dataproductos=ArrayHelper::map($productos, 'codigo_producto', 'concepto_producto');
echo $form->field($model, 'codigo_producto')->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PRODUCTO'])?>

     <?php $cambios= Cambios::find()->where('baja=0')->all();
$datacambios=ArrayHelper::map($cambios, 'codigo_cambio', 'motivo_cambio');
echo $form->field($model, 'codigo_cambio')->dropDownList($datacambios, ['prompt'=> 'ELIGE UN CAMBIO'])?>

    <?= $form->field($model, 'cantidad')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
