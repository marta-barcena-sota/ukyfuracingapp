<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cambios */

$this->title = 'Update Cambios: ' . $model->codigo_cambio;
$this->params['breadcrumbs'][] = ['label' => 'Cambios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_cambio, 'url' => ['view', 'id' => $model->codigo_cambio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cambios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
