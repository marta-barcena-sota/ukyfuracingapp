<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cambios */

$this->title = 'ALTA CAMBIO';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'CAMBIOS', 'url' => ['cambios/cambioslogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="cambios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
