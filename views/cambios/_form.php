<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cambios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cambios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'motivo_cambio', ['template' => '{label}
*{input}'])->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta-guardar']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
