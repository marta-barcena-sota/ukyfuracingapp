<?php

/* @var $this yii\web\View */
/* @var $model app\models\Cambios */


use yii\helpers\Html;

$this->title = 'CAMBIOS';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="body-content">
    <br>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/cambiosalmacen.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>CAMBIOS DE PRODUCTOS EN ALMACEN</h1>
                    
                </div>
                
            </div>
        </div>
        
        <br>
        <br>
        
        <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('Nuevo', ['cambios/create'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['cambios/cambioslogisticapapelera'], ['class' => 'btn btn-marta']) ?>
                 
                      <?= Html::a('Añadir comentario a cambio', ['comentarios/createcambios'], ['class' => 'btn btn-marta']) ?>
    </p>    
            </div>
        </div>
        
        <br>
        <br>
    </div>

<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_pedido',
         'motivo_cambio',
     ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                   'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            } ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_cambio'];

        	return $url;

    	}
                if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_cambio'];

        	return $url;

    	}


	}]
         
        ]]);?>
