<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueParticipanEnProyectos */

$this->title = 'Update Proveedores Que Participan En Proyectos: ' . $model->codigo_participacion;
$this->params['breadcrumbs'][] = ['label' => 'Producción: Menú', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = ['label' => 'PROYECTOS', 'url' => ['proyectos/proyectosproduccion']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="proveedores-que-participan-en-proyectos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
