<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;
use app\models\Proyectos;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueParticipanEnProyectos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-participan-en-proyectos-form">

    <?php $form = ActiveForm::begin(); ?>

       <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])?>

<?php $proyectos= Proyectos::find()->where('baja=0')->all();
$dataproyectos=ArrayHelper::map($proyectos, 'codigo_proyecto', 'nombre_proyecto');
echo $form->field($model, 'codigo_proyecto')->dropDownList($dataproyectos, ['prompt'=> 'ELIGE UN PROYECTO'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
