<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Intermediarios;

/* @var $this yii\web\View */
/* @var $model app\models\EnlacesIntermediarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enlaces-intermediarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $intermediarios= Intermediarios::find()->where('baja=0')->all();
$dataintermediarios=ArrayHelper::map($intermediarios, 'codigo_intermediario', 'nombre_intermediario');
echo $form->field($model, 'codigo_intermediario')->dropDownList($dataintermediarios, ['prompt'=> 'ELIGE UN INTERMEDIARIO'])?>

    <?= $form->field($model, 'enlace_intermediario')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
