<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EnlacesIntermediarios */

$this->title = 'Create Enlaces Intermediarios';
$this->params['breadcrumbs'][] = ['label' => 'Enlaces Intermediarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enlaces-intermediarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
