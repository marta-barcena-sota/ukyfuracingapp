<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Provproductosform */

$this->title = 'ALTA PROVEEDOR DE PRODUCTO';
$this->params['breadcrumbs'][] = ['label' => 'Logística: Menú', 'url' => ['site/menulogistica']];
$this->params['breadcrumbs'][] = ['label' => 'PROVEEDORES DE PRODUCTOS', 'url' => ['proveedores/proveedoresproductoslogistica']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="proveedores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formprovproductos', [
        'model' => $model,
    ]) ?>

</div>
