<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transportes */

$this->title = 'Update Transportes: ' . $model->codigo_transporte;
$this->params['breadcrumbs'][] = ['label' => 'Transportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_transporte, 'url' => ['view', 'id' => $model->codigo_transporte]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transportes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
