<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedores;
use app\models\OrdenesDeFabricacion;
/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresQueColaboranEnOrdenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-que-colaboran-en-ordenes-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php $proveedores= Proveedores::find()->where('baja=0')->all();
$dataproveedores=ArrayHelper::map($proveedores, 'codigo_proveedor', 'nombre_proveedor');
echo $form->field($model, 'codigo_proveedor')->dropDownList($dataproveedores, ['prompt'=> 'ELIGE UN PROVEEDOR'])?>

  <?php $ordenesdefabricacion= OrdenesDeFabricacion::find()->where('baja=0')->all();
$dataordenesdefabricacion=ArrayHelper::map($ordenesdefabricacion, 'codigo_orden_de_fabricacion', 'nombre_orden_de_fabricacion');
echo $form->field($model, 'codigo_orden_fabricacion')->dropDownList($dataordenesdefabricacion, ['prompt'=> 'ELIGE UNA ORDEN DE FABRICACION']) ?>

    <?= $form->field($model, 'motivo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
