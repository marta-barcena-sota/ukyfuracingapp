<?php

/* @var $this yii\web\View */
/* @var $model app\models\Materiales */


use yii\helpers\Html;

$this->title = 'MATERIALES';
$this->params['breadcrumbs'][] = ['label' => 'PRODUCCIÓN', 'url' => ['site/menuproduccion']];
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="body-content">
        <br><!-- comment -->
        <br><!-- comment -->
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                     <?= Html::img('@web/images/materiales.png', ['alt' => 'My logo']) ?>
                    
                </div>
            </div>
            <div class="col-sm-8">
                
                <div class="thumbnail">
                    
                    <h1>MATERIALES</h1>
                    
                </div>
                
            </div>
        </div>
        <br><!-- comment -->
        <br><!-- comment -->
        <div class="row">
            
              <div class="col-sm-6">
             <p>
        <?= Html::a('Registrar un nuevo material', ['create'], ['class' => 'btn btn-marta']) ?>
            
                      <?= Html::a('Papelera', ['materiales/materialesproduccionpapelera'], ['class' => 'btn btn-marta']) ?>
    </p>    
            </div>
        </div>
        <br><!-- comment -->
        <br><!-- comment -->
    </div>
  
<?= \yii\grid\GridView::widget([
    'dataProvider'=> $resultados,
     'columns' => [
//         ['class'=>'yii\grid\SerialColumn'],
//         'codigo_material',
         'familia_material',
         'clase_material',
         'tipo_material',
         'carpeta_material',
         ['class'=>'yii\grid\ActionColumn',
              'template' => '{view}, {baja}',
               'buttons' => [
            'view' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url, [
                            'title' => Yii::t('app', 'lead-view')]);
            },
                    'baja' => function ($url) {
              
                return Html::a('<span class="glyphicon glyphicon-remove"></span>',$url, [
                            'title' => Yii::t('app', 'lead-baja'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to update this item?'),
            'data-method'  => 'post']);
            }
            ],
                    'urlCreator' => function ($action, $model) {

    	if ($action === 'view') {

        	$url = './view?id='.$model['codigo_material'];

        	return $url;

    	}
        
        if ($action === 'baja') {

        	$url = './baja?id='.$model['codigo_material'];

        	return $url;

    	}

	}]
         ],
      
        ]);?>
