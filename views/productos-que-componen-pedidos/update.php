<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenPedidos */

$this->title = 'Update Productos Que Componen Pedidos: ' . $model->codigo_componente;
$this->params['breadcrumbs'][] = ['label' => 'Productos Que Componen Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_componente, 'url' => ['view', 'id' => $model->codigo_componente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-que-componen-pedidos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
