<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Productos;
use app\models\Pedidos;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosQueComponenPedidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-que-componen-pedidos-form">

    <?php $form = ActiveForm::begin(); ?>

 <?php $productos= Productos::find()->where('baja=0')->all();
$dataproductos=ArrayHelper::map($productos, 'codigo_producto', 'concepto_producto');
echo $form->field($model, 'codigo_producto')->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PRODUCTO'])?>

    <?php $pedidos= Pedidos::find()->where('baja=0')->all();
$datapedidos=ArrayHelper::map($pedidos, 'codigo_pedido', 'concepto_pedido');
echo $form->field($model, 'codigo_pedido')->dropDownList($dataproductos, ['prompt'=> 'ELIGE UN PEDIDO'])?>


    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'cantidad_pendiente')->textInput() ?>

    <?= $form->field($model, 'realizado')->textInput()->dropDownList(
            [0=>'NO',
                1=> 'SÍ',
            ]
            , ['prompt'=> '¿ESTÁ REALIZADO?']) ?>

  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-marta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
