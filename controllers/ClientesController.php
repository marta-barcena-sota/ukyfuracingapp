<?php

namespace app\controllers;

use Yii;
use app\models\Clientes;
use app\models\ClientesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ClientesController implements the CRUD actions for Clientes model.
 */
class ClientesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clientes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->pagination = ['pageSize' => 8];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clientes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clientes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_cliente]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_cliente]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
  public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['site/index']);
    }

     public function actionBaja($id)
    {
         Yii::$app->db->createCommand()
        ->update('clientes', ['baja'=>1], 'codigo_cliente='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionAlta($id)
    {
         Yii::$app->db->createCommand()
        ->update('clientes', ['baja'=>0], 'codigo_cliente='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
       public function actionClientesopciones(){
        return $this->render('clientesopciones');
    
    }
    
         public function actionClientesenalta (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Clientes::find()-> select("codigo_cliente, pais_cliente, cif_cliente, nombre_cliente")
            -> where ('baja=0'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("clientesenalta",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_cliente', 'pais_cliente', 'cif_cliente', 'nombre_cliente'],
            "titulo"=> "CLIENTES DADOS DE ALTA",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS FALSE ",
            
        ]);
     }
     
          public function actionClientesenbaja (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Clientes::find()-> select("codigo_cliente, pais_cliente, cif_cliente, nombre_cliente")
            -> where ('baja=1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("clientesenbaja",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_cliente', 'pais_cliente', 'cif_cliente', 'nombre_cliente'],
            "titulo"=> "CLIENTES DADOS DE BAJA",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS TRUE ",
            
        ]);
     }

    /**
     * Finds the Clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clientes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionClienteslogistica (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Clientes::find()-> select("codigo_cliente, nombre_cliente, pais_cliente, cif_cliente")
            -> where ('baja=0'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("clienteslogistica",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_cliente','nombre_cliente', 'pais_cliente', 'cif_cliente'],
            "titulo"=> "CLIENTES",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionClienteslogisticabajas (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Clientes::find()-> select("codigo_cliente, nombre_cliente, pais_cliente, cif_cliente")
            -> where ('baja=1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("clienteslogisticabajas",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_cliente','nombre_cliente', 'pais_cliente', 'cif_cliente'],
            "titulo"=> "CLIENTES",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
}
