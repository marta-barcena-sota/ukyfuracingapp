<?php

namespace app\controllers;

use Yii;
use app\models\Productos;
use app\models\ProductosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ProductosController implements the CRUD actions for Productos model.
 */
class ProductosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Productos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Productos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Productos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Productos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_producto]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Productos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_producto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Productos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['site/index']);
    }

     public function actionBaja($id)
    {
         Yii::$app->db->createCommand()
        ->update('productos', ['baja'=>1], 'codigo_producto='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionAlta($id)
    {
         Yii::$app->db->createCommand()
        ->update('productos', ['baja'=>0], 'codigo_producto='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Finds the Productos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Productos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Productos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionInventariologistica (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Productos::find()-> select("codigo_producto, referencia_articulo_producto, referencia_interna_producto,"
                    . "concepto_producto, concepto_producto, primera_categoria_producto,"
                    . "segunda_categoria_producto, tercera_categoria_producto, cantidad_en_stock,"
                    . "forma_de_almacenamiento")
            -> where ('baja=0'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("inventariologistica",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_producto','referencia_articulo_producto', 'referencia_interna_producto', 'concepto_producto', 'primera_categoria_producto'
                , 'segunda_categoria_producto', 'tercera_categoria_producto', 'cantidad_en_stock', 'forma_de_almacenamiento'],
            "titulo"=> "INVENTARIO",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionInventariologisticabajas (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Productos::find()-> select("codigo_producto, referencia_articulo_producto, referencia_interna_producto,"
                    . "concepto_producto, concepto_producto, primera_categoria_producto,"
                    . "segunda_categoria_producto, tercera_categoria_producto, cantidad_en_stock,"
                    . "forma_de_almacenamiento")
            -> where ('baja=1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("inventariologisticabajas",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_producto','referencia_articulo_producto', 'referencia_interna_producto', 'concepto_producto', 'primera_categoria_producto'
                , 'segunda_categoria_producto', 'tercera_categoria_producto', 'cantidad_en_stock', 'forma_de_almacenamiento'],
            "titulo"=> "INVENTARIO",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
}
