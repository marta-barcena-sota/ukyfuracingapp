<?php

namespace app\controllers;

use Yii;
use app\models\Recepciones;
use app\models\Pedidos;
use app\controllers\PedidosController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

class RecepcionesController extends PedidosController
{
      public function actionCreaterecepciones()
    {
        $model = new Recepciones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_pedido]);
        }

        return $this->render('createrecepciones', [
            'model' => $model,
        ]);
    }
    
    public function actionRecepcioneslogistica(){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("codigo_pedido, tracking_pedido, estado_pedido, "
                    . "urgencia_pedido, forma_de_pago")
            -> where ('baja=0')
                ->Andwhere('tipo_pedido="R"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("recepcioneslogistica",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_pedido','tracking_pedido',  'estado_pedido', 'urgencia_pedido', 'forma_de_pago'],
            "titulo"=> "BASE DE DATOS GENERAL",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }     
    
      public function actionRecepcioneslogisticapapelera(){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("codigo_pedido, tracking_pedido, estado_pedido, "
                    . "urgencia_pedido, forma_de_pago")
            -> where ('baja=1')
                ->Andwhere('tipo_pedido="R"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("recepcioneslogisticapapelera",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_pedido','tracking_pedido',  'estado_pedido', 'urgencia_pedido', 'forma_de_pago'],
            "titulo"=> "BASE DE DATOS GENERAL",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }     

}