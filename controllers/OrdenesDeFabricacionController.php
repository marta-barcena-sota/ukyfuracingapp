<?php

namespace app\controllers;

use Yii;
use app\models\OrdenesDeFabricacion;
use app\models\OrdenesDeFabricacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * OrdenesDeFabricacionController implements the CRUD actions for OrdenesDeFabricacion model.
 */
class OrdenesDeFabricacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrdenesDeFabricacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdenesDeFabricacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrdenesDeFabricacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrdenesDeFabricacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrdenesDeFabricacion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_orden_de_fabricacion]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrdenesDeFabricacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_orden_de_fabricacion]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrdenesDeFabricacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['site/index']);
    }
    
       public function actionBaja($id)
    {
         Yii::$app->db->createCommand()
        ->update('ordenes_de_fabricacion', ['baja'=>1], 'codigo_orden_de_fabricacion='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionAlta($id)
    {
         Yii::$app->db->createCommand()
        ->update('ordenes_de_fabricacion', ['baja'=>0], 'codigo_orden_de_fabricacion='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the OrdenesDeFabricacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenesDeFabricacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenesDeFabricacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
        public function actionOrdenesproduccion (){
        $dataProvider= new ActiveDataProvider([
            'query'=> OrdenesDeFabricacion::find()-> select("codigo_orden_de_fabricacion, nombre_orden_de_fabricacion")
            -> where ('baja=0'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("ordenesproduccion",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_orden_de_fabricacion','nombre_orden_de_fabricacion'],
            "titulo"=> "VENTAS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionOrdenesproduccionpapelera (){
        $dataProvider= new ActiveDataProvider([
            'query'=> OrdenesDeFabricacion::find()-> select("codigo_orden_de_fabricacion, nombre_orden_de_fabricacion")
            -> where ('baja=1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("ordenesproduccionpapelera",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_orden_de_fabricacion','nombre_orden_de_fabricacion'],
            "titulo"=> "VENTAS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
}
