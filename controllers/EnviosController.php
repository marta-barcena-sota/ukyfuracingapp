<?php

namespace app\controllers;

use Yii;
use app\models\Envios;
use app\models\Pedidos;
use app\controllers\PedidosController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

class EnviosController extends PedidosController
{
      public function actionCreateenvios()
    {
        $model = new Envios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_pedido]);
        }

        return $this->render('createenvios', [
            'model' => $model,
        ]);
    }
    
     public function actionEnvioslogistica(){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("codigo_pedido, tracking_pedido, estado_pedido, "
                    . "urgencia_pedido, forma_de_pago")
            -> where ('baja=0')
                ->Andwhere('tipo_pedido="E"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("envioslogistica",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_pedido','tracking_pedido',  'estado_pedido', 'urgencia_pedido', 'forma_de_pago'],
            "titulo"=> "BASE DE DATOS GENERAL",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }     
    
     public function actionEnvioslogisticapapelera(){
        $dataProvider= new ActiveDataProvider([
            'query'=> Pedidos::find()-> select("codigo_pedido, tracking_pedido, estado_pedido, "
                    . "urgencia_pedido, forma_de_pago")
            -> where ('baja=1')
                ->Andwhere('tipo_pedido="E"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("envioslogisticapapelera",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_pedido','tracking_pedido',  'estado_pedido', 'urgencia_pedido', 'forma_de_pago'],
            "titulo"=> "BASE DE DATOS GENERAL",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }     

}