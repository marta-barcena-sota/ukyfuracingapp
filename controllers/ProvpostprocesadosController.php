<?php

namespace app\controllers;

use Yii;
use app\models\Provpostprocesados;
use app\controllers\ProveedoresController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ProveedoresController implements the CRUD actions for Proveedores model.
 */
class ProvpostprocesadosController extends ProveedoresController
{
      public function actionCreateprovpostprocesados()
    {
        $model = new Provpostprocesados();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_proveedor]);
        }

        return $this->render('createprovpostprocesados', [
            'model' => $model,
        ]);
    }
    
//     public function actionViewprovproductos($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }
}
