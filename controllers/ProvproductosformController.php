<?php

namespace app\controllers;

use Yii;
use app\models\Provproductosform;
//use app\models\ProveedoresSearch;
use app\controllers\ProveedoresController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ProveedoresController implements the CRUD actions for Proveedores model.
 */
class ProvproductosformController extends ProveedoresController
{
      public function actionCreateprovproductos()
    {
        $model = new Provproductosform();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_proveedor]);
        }

        return $this->render('createprovproductos', [
            'model' => $model,
        ]);
    }
    
//     public function actionViewprovproductos($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }
}
