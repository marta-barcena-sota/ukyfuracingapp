<?php

namespace app\controllers;

use Yii;
use app\models\Proveedores;
use app\models\ProveedoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ProveedoresController implements the CRUD actions for Proveedores model.
 */
class ProveedoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proveedores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProveedoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 8];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proveedores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proveedores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Proveedores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_proveedor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
  

    /**
     * Updates an existing Proveedores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_proveedor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Proveedores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
     public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['site/index']);
    }

     public function actionBaja($id)
    {
         Yii::$app->db->createCommand()
        ->update('proveedores', ['baja'=>1], 'codigo_proveedor='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionAlta($id)
    {
         Yii::$app->db->createCommand()
        ->update('proveedores', ['baja'=>0], 'codigo_proveedor='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionOpcionesprov(){
        return $this->render('opcionesprov');
    
    }

    /**
     * Finds the Proveedores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proveedores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proveedores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
////    public function actionAlta() {
////   $model = new ProveedoresForm();
////   return $this->render('_form', ['model' => $model]);
//}
    
     public function actionProveedoresenalta (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, cif_proveedor, eori_proveedor, nombre_proveedor")
            -> where ('baja=0'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedoresenalta",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor', 'cif_proveedor', 'eori_proveedor', 'nombre_proveedor'],
            "titulo"=> "ALTAS DE PROVEEDORES",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS FALSE ",
            
        ]);
     }
     
      public function actionProveedoresenbaja (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, cif_proveedor, eori_proveedor, nombre_proveedor")
            -> where ('baja=1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedoresenbaja",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor', 'cif_proveedor', 'eori_proveedor', 'nombre_proveedor'],
            "titulo"=> "PROVEEDORES DADOS DE BAJA",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS TRUE ",
            
        ]);
     }
     
     public function actionProveedorestransportelogistica (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=0')
                -> andWhere('tipo_proveedor="TRANSPORTE"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedorestransportelogistica",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE TRANSPORTE",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
          public function actionProveedorestransportelogisticabajas (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=1')
                -> andWhere('tipo_proveedor="TRANSPORTE"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedorestransportelogisticabajas",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE TRANSPORTE",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionProveedoresproductoslogistica (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=0')
                -> andWhere('tipo_proveedor="PRODUCTOS"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedoresproductoslogistica",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
        public function actionProveedoresproductoslogisticabajas (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=1')
                -> andWhere('tipo_proveedor="PRODUCTOS"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedoresproductoslogisticabajas",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionProvpostprocesadosproduccion (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=0')
                -> andWhere('tipo_proveedor="POSTPROCESADOS"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("provpostprocesadosproduccion",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
      public function actionProvpostprocesadosproduccionbajas (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=1')
                -> andWhere('tipo_proveedor="POSTPROCESADOS"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("provpostprocesadosproduccionbajas",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionProvfabricacionproduccion (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=0')
                -> andWhere('tipo_proveedor="FABRICACION"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedoresfabricacionproduccion",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionProvfabricacionproduccionpapelera (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=1')
                -> andWhere('tipo_proveedor="FABRICACION"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedoresfabricacionproduccionpapelera",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionProvmateriales (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=0')
                -> andWhere('tipo_proveedor="MATERIALES"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("proveedoresmaterialesproduccion",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionProvmaterialesbajas (){
        $dataProvider= new ActiveDataProvider([
            'query'=> Proveedores::find()-> select("codigo_proveedor, nombre_proveedor, cif_proveedor, tipo_de_empresa_proveedor, pais_proveedor")
            -> where ('baja=1')
                -> andWhere('tipo_proveedor="MATERIALES"'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("provmaterialesproduccionbajas",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_proveedor','nombre_proveedor', 'cif_proveedor', 'tipo_de_empresa_proveedor', 'pais_proveedor'],
            "titulo"=> "PROVEEDORES DE PRODUCTOS",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM productos WHERE baja IS FALSE ",
            
        ]);
     }
}
