<?php

namespace app\controllers;

use Yii;
use app\models\TlfsIntermediarios;
use app\models\TlfsIntermediariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * TlfsIntermediariosController implements the CRUD actions for TlfsIntermediarios model.
 */
class TlfsIntermediariosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TlfsIntermediarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TlfsIntermediariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TlfsIntermediarios model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TlfsIntermediarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TlfsIntermediarios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_tlf]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TlfsIntermediarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_tlf]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TlfsIntermediarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['site/index']);
    }

    public function actionBaja($id)
    {
         Yii::$app->db->createCommand()
        ->update('tlfs_intermediarios', ['baja'=>1], 'codigo_tlf='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionAlta($id)
    {
         Yii::$app->db->createCommand()
        ->update('tlfs_intermediarios', ['baja'=>0], 'codigo_tlf='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Finds the TlfsIntermediarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TlfsIntermediarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TlfsIntermediarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
      public function actionIntermediariostlfs (){
        $dataProvider= new ActiveDataProvider([
            'query'=> TlfsIntermediarios::find()-> select("codigo_tlf, tlf_intermediario")
            -> where ('baja=0'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("intermediariostlfs",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_tlf', 'tlf_intermediario'],
            "titulo"=> "ALTAS DE PROVEEDORES",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS FALSE ",
            
        ]);
     }
     
     public function actionIntermediariostlfspapelera (){
        $dataProvider= new ActiveDataProvider([
            'query'=> TlfsIntermediarios::find()-> select("codigo_tlf, tlf_intermediario")
            -> where ('baja=1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("intermediariostlfspapelera",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_tlf', 'tlf_intermediario'],
            "titulo"=> "ALTAS DE PROVEEDORES",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS FALSE ",
            
        ]);
     }
}
