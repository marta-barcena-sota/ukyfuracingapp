<?php

namespace app\controllers;

use Yii;
use app\models\Provtransporteform;
//use app\models\ProveedoresSearch;
use app\controllers\ProveedoresController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ProveedoresController implements the CRUD actions for Proveedores model.
 */
class ProvtransporteformController extends ProveedoresController
{
      public function actionCreateprovtransporte()
    {
        $model = new Provtransporteform();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_proveedor]);
        }

        return $this->render('createprovtransporte', [
            'model' => $model,
        ]);
    }
}
