<?php

namespace app\controllers;

use Yii;
use app\models\EmailsClientes;
use app\models\EmailsClientesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * EmailsClientesController implements the CRUD actions for EmailsClientes model.
 */
class EmailsClientesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailsClientes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailsClientesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmailsClientes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmailsClientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmailsClientes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_email]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EmailsClientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_email]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmailsClientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
      public function actionBaja($id)
    {
         Yii::$app->db->createCommand()
        ->update('emails_clientes', ['baja'=>1], 'codigo_email='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionAlta($id)
    {
         Yii::$app->db->createCommand()
        ->update('emails_clientes', ['baja'=>0], 'codigo_email='.$id)
        ->execute();
    return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the EmailsClientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailsClientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailsClientes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
     public function actionEmailsclientes (){
        $dataProvider= new ActiveDataProvider([
            'query'=> EmailsClientes::find()-> select("codigo_email, email_cliente")
            -> where ('baja=0'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("emailsclientesagenda",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_email', 'email_empleado'],
            "titulo"=> "ALTAS DE PROVEEDORES",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS FALSE ",
            
        ]);
     }
     
        public function actionEmailsclientespapelera (){
        $dataProvider= new ActiveDataProvider([
            'query'=> EmailsClientes::find()-> select("codigo_email, email_cliente")
            -> where ('baja=1'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this-> render("emailsclientesagendapapelera",[
            "resultados"=>$dataProvider,
            "campos"=>['codigo_email', 'email_empleado'],
            "titulo"=> "ALTAS DE PROVEEDORES",
             "enunciado"=> "CASO DE USO UTILIZANDO ORM",
            "sql"=> "SELECT * FROM proveedores WHERE baja IS FALSE ",
            
        ]);
     }
}
